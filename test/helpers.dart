import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:tic_tac_toe/models/PlayerModel.dart';
import 'package:tic_tac_toe/models/TeamModel.dart';

Iterable<TeamModel> get mockTeams => [
      TeamModel(
        teamImage: null,
        teamTag: "Team 1",
      ),
      TeamModel(
        teamImage: null,
        teamTag: "Team 2",
      ),
      TeamModel(
        teamImage: null,
        teamTag: "Team 3",
      ),
    ];

Iterable<PlayerModel> get mockPlayers => [
      LocalPlayer("Player A"),
      LocalPlayer("Player B"),
      LocalPlayer("Player C"),
      LocalPlayer("Player D"),
    ];

Widget wrapInScaffold(Widget widget) => MaterialApp(
      home: Scaffold(
        body: widget,
      ),
    );

WidgetTester pumpWrappedWidget(WidgetTester tester, {Widget widget}) =>
    tester..pumpWidget(wrapInScaffold(widget));
