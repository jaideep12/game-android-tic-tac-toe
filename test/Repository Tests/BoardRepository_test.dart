import 'dart:math';

import 'package:flutter_test/flutter_test.dart';
import 'package:optional/optional.dart';
import 'package:tic_tac_toe/models/TeamModel.dart';
import 'package:tic_tac_toe/repositories/BoardRepository.dart';

void main() {
  group("BoardRepository tests", () {
    var boardRepo = BoardRepository(boardRectangle: Rectangle(1, 1, 2, 2));
    var team = TeamModel(
      teamTag: "test_team",
      teamImage: null,
    );
    var team2 = TeamModel(
      teamTag: "test_team_2",
      teamImage: null,
    );

    test("setTeamAt", () {
      expect(boardRepo.setTeamAt(Point(1, 1), team), isTrue);
      expect(boardRepo.getTeamAt(Point(1, 1)).value, team);

      expect(boardRepo.setTeamAt(Point(1, 1), team2), isFalse);
      expect(boardRepo.getTeamAt(Point(1, 1)).value, team);

      expect(boardRepo.grid.toSet().length, 2);
    });

    test("force setTeamAt", () {
      expect(
          boardRepo.setTeamAt(
            Point(1, 1),
            team2,
            forceUpdate: true,
          ),
          isTrue);
      expect(boardRepo.getTeamAt(Point(1, 1)).value, team2);
      expect(boardRepo.grid.toSet().length, 2);
    });

    test("clear board point", () {
      print("before clear point: ${boardRepo.getTeamAt(Point(1, 1))}");
      expect(boardRepo.clearBoardPoint(Point(1, 1)), isTrue);
      print("after clear point: ${boardRepo.getTeamAt(Point(1, 1))}");
      expect(boardRepo.clearBoardPoint(Point(1, 1)), isFalse);
      expect(boardRepo.getTeamAt(Point(1, 1)), const Optional.empty());
      expect(boardRepo.grid.toSet().length, 1);
    });

    test("clear board", () {
      expect(boardRepo.setTeamAt(Point(1, 1), team), isTrue);
      expect(boardRepo.getTeamAt(Point(1, 1)).value, team);

      expect(boardRepo.setTeamAt(Point(1, 2), team2), isTrue);
      expect(boardRepo.getTeamAt(Point(1, 2)).value, team2);

      print("before clear board: ${boardRepo.grid}");
      expect(boardRepo.clearBoard(), 2);
      print("after clear board: ${boardRepo.grid}");
      expect(boardRepo.clearBoard(), 0);
      expect(boardRepo.grid.toSet().length, 1);
    });
  });
}
