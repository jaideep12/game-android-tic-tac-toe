import 'package:flutter_test/flutter_test.dart';
import 'package:optional/optional.dart';
import 'package:tic_tac_toe/models/PlayerModel.dart';
import 'package:tic_tac_toe/models/TeamModel.dart';
import 'package:tic_tac_toe/repositories/PlayersTeamsRepository.dart';

import '../helpers.dart';

void main() {
  group("PlayersTeamsRepo independent players/teams tests", () {
    PlayersTeamsRepository repo = PlayersTeamsRepository(
      teams: mockTeams,
      players: mockPlayers,
    );

    test("add new team test", () {
      int prev = repo.teams.length;
      expect(repo.addNewTeam(mockTeams.first), isFalse);
      expect(
        repo.addNewTeam(TeamModel(teamImage: null, teamTag: "test_team")),
        isTrue,
      );
      expect(repo.teams.length, prev + 1);
    });

    test("remove team test", () {
      int prev = repo.teams.length;
      expect(repo.removeTeam(null), isFalse);
      expect(
        repo.removeTeam(TeamModel(teamImage: null, teamTag: "test_team")),
        isTrue,
      );
      expect(repo.teams.length, prev - 1);
    });

    test("add new player test", () {
      int prev = repo.players.length;
      expect(repo.addNewPlayer(null), isFalse);
      expect(repo.addNewPlayer(mockPlayers.first), isFalse);
      expect(
        repo.addNewPlayer(LocalPlayer("test_player")),
        isTrue,
      );
      expect(repo.players.length, prev + 1);
    });

    test("remove player test", () {
      int prev = repo.players.length;
      expect(repo.removePlayer(null), isFalse);
      expect(
        repo.removePlayer(LocalPlayer("test_player")),
        isTrue,
      );
      expect(repo.players.length, prev - 1);
    });
  });

  group("PlayersTeamsRepo dependent players/teams tests", () {
    PlayersTeamsRepository repo = PlayersTeamsRepository(
      teams: mockTeams,
      players: mockPlayers,
    );

    test("add existing player to team", () {
      var player = mockPlayers.first;
      var team = mockTeams.first;
      var teamL = repo.getTeamPlayerList(team).value;
      int prevL = teamL.length;

      expect(repo.movePlayerToTeam(null, team), isFalse);
      expect(repo.movePlayerToTeam(player, null), isFalse);
      expect(repo.movePlayerToTeam(null, null), isFalse);
      expect(repo.movePlayerToTeam(player, team), isTrue);
      expect(repo.movePlayerToTeam(player, team), isFalse);

      expect(teamL.contains(player), isTrue);
      expect(repo.getTeamPlayerList(team).value.contains(player), isTrue);
      expect(teamL.length, prevL + 1);
    });

    test("add new player to team", () {
      var player = LocalPlayer("test_player");
      var team = mockTeams.first;
      var teamL = repo.getTeamPlayerList(team).value;
      int prevL = teamL.length;

      expect(repo.movePlayerToTeam(player, team), isFalse);
      expect(repo.addNewPlayer(player, team: team), isTrue);
      expect(repo.addNewPlayer(player, team: team), isFalse);

      expect(teamL.contains(player), isTrue);
      expect(repo.getTeamPlayerList(team).value.contains(player), isTrue);
      expect(teamL.length, prevL + 1);
    });

    test("add new player to new team", () {
      var player = LocalPlayer("test_player_2");
      var team = TeamModel(teamTag: "test_team", teamImage: null);
      var teamC = repo.teams.length;

      expect(repo.movePlayerToTeam(player, team), isFalse);
      expect(repo.addNewPlayer(player, team: team), isTrue);
      expect(repo.addNewPlayer(player, team: team), isFalse);

      var teamL = repo.getTeamPlayerList(team).value;
      expect(teamL.contains(player), isTrue);
      expect(teamL.length, 1);
      expect(repo.teams.contains(team), isTrue);
      expect(repo.teams.length, teamC + 1);
    });

    test("remove player from team", () {
      var player = LocalPlayer("test_player");
      var team = mockTeams.first;
      var teamL = repo.getTeamPlayerList(team).value;
      var prevL = teamL.length;

      expect(repo.removePlayerFromTeam(null), isFalse);
      expect(repo.removePlayerFromTeam(player), isTrue);
      expect(repo.removePlayerFromTeam(player), isFalse);

      expect(teamL.contains(player), isFalse);
      expect(teamL.length, prevL - 1);
    });

    test("remove player", () {
      var player = LocalPlayer("test_player_2");
      var team = repo.getPlayerTeam(player).value;
      var teamL = repo.getTeamPlayerList(team).value;
      var prevL = teamL.length;
      var playerL = repo.players.length;

      expect(repo.removePlayer(null), isFalse);
      expect(repo.removePlayer(player), isTrue);
      expect(repo.removePlayer(player), isFalse);

      expect(teamL.contains(player), isFalse);
      expect(repo.players.contains(player), isFalse);
      expect(teamL.length, prevL - 1);
      expect(repo.players.length, playerL - 1);
    });

    test("remove team", () {
      var team = repo.teams.first;
      var playerL = repo.getTeamPlayerList(team).value;
      print("team: $team");
      print("team players: $playerL");

      expect(repo.removeTeam(null), isFalse);
      expect(repo.removeTeam(team), isTrue);
      expect(repo.removeTeam(team), isFalse);

      for (var player in playerL) {
        expect(repo.getPlayerTeam(player), const Optional.empty());
      }
      expect(repo.teams.contains(team), isFalse);
    });
  });
}
