import 'dart:math';

import 'package:flutter_test/flutter_test.dart';
import 'package:optional/optional.dart';
import 'package:tic_tac_toe/math_helper.dart';
import 'package:tic_tac_toe/models/StrikeModel.dart';

void main() {
  group("StrikeModel test", () {
    StrikeModel point = StrikeModel.pointStrike(start: Point(1, 1)),
        line = StrikeModel.lineStrike(
      start: Point(1, 1),
      direction: Direction_8_Way.NORTH,
      length: 1,
    );

    test("point strike attrib test", () async {
      expect(point.start, Point(1, 1));
      expect(point.endPoint, Point(1, 1));
      expect(point.strikeLength, 1);
      expect(point.strikePoints.toList(), const [Point(1, 1)]);
    });

    test("line strike attrib test", () async {
      expect(point.start, Point(1, 1));
      expect(point.endPoint, Point(1, 1));
      expect(point.strikeLength, 1);
      expect(point.strikePoints.toList(), const [Point(1, 1)]);
    });

    test("point strike extend test", () async {
      var exP = point.extended(Direction_8_Way.NORTH).value;
      expect((exP as LineStrike).direction, Direction_8_Way.NORTH);
      expect(exP.start, Point(1, 1));
      expect(exP.endPoint, Point(1, 2));
      expect(exP.strikeLength, 2);
      expect(exP.strikePoints.toList(), const [Point(1, 1), Point(1, 2)]);
    });

    test("line strike extend test", () async {
      Direction_8_Way.values
          .takeWhile((dir) => dir != (line as LineStrike).direction)
          .forEach((dir) {
        expect(line.extended(dir), const Optional.empty());
      });
      var exP = line.extended(Direction_8_Way.NORTH).value;
      expect(exP.start, Point(1, 1));
      expect(exP.endPoint, Point(1, 2));
      expect(exP.strikeLength, 2);
      expect(exP.strikePoints.toList(), const [Point(1, 1), Point(1, 2)]);
    });
  });
}
