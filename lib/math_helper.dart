import 'dart:math';

import 'package:flutter/material.dart';

enum Direction_8_Way {
  NORTH,
  NORTH_EAST,
  EAST,
  SOUTH_EAST,
  SOUTH,
  SOUTH_WEST,
  WEST,
  NORTH_WEST,
}

extension DirectionExt on Direction_8_Way {
  Direction_8_Way get opposite {
    switch (this) {
      case Direction_8_Way.NORTH:
        return Direction_8_Way.SOUTH;
      case Direction_8_Way.NORTH_EAST:
        return Direction_8_Way.SOUTH_WEST;
      case Direction_8_Way.EAST:
        return Direction_8_Way.WEST;
      case Direction_8_Way.SOUTH_EAST:
        return Direction_8_Way.NORTH_WEST;
      case Direction_8_Way.SOUTH:
        return Direction_8_Way.NORTH;
      case Direction_8_Way.SOUTH_WEST:
        return Direction_8_Way.NORTH_EAST;
      case Direction_8_Way.WEST:
        return Direction_8_Way.EAST;
      case Direction_8_Way.NORTH_WEST:
        return Direction_8_Way.SOUTH_EAST;
    }
    return null;
  }
}

extension PointExt<T extends num> on Point<T> {
  /// Unit vector in +ve x-axis and y-axis.
  static const Point<num> UNIT_VECTOR = const Point<num>(1, 1);

  /// Unit vector in +ve x-axis.
  static const Point<num> UNIT_VECTOR_X = const Point<num>(1, 0);

  /// Unit vector in +ve y-axis.
  static const Point<num> UNIT_VECTOR_Y = const Point<num>(0, 1);

  /// Returns the manhattan distance to the other point.
  T manhattanDistanceTo(Point<T> other) => this.l1NormDistanceTo(other) as T;

  /// Returns the hamming distance to the other point.
  T hammingDistanceTo(Point<T> other) => this.l0NormDistanceTo(other) as T;

  /// Returns a new point mirrored along main diagonal with (x,-y)
  Point<T> get swapped => Point(y, x);
  Point<T> get mirrored_diagonal => Point(y, x);

  /// Scales the point by multiplying its x and y values with given scaling.
  Point<T> scaled(T scaling) => Point(x * scaling, y * scaling);

  /// Returns a new point mirrored along cross diagonal with (x,-y)
  Point<T> get mirrored_cross_diagonal => Point(-x, -y);

  /// Returns a new point mirrored along x-axis with (x,-y)
  Point<T> get mirrored_x => Point(x, -y);

  /// Returns a new point mirrored along y-axis with (x,-y)
  Point<T> get mirrored_y => Point(-x, y);

  /// Returns a new point with x.abs() and y.abs()
  Point<T> get absolute => Point(x.abs() as T, y.abs() as T);

  bool get isOnDiagonal => x.abs() == y.abs();
  bool get isOnMainDiagonal => x == y;
  bool get isOnCrossDiagonal => x == -y;

  /// Returns true if x-distance and y-distance to other point is equal.
  bool isDiagonalTo(Point<T> other) => (this - other).isOnDiagonal;

  /// Returns the l-0 norm metric of this point's co-ordinates.
  /// This is the magnitude of the co-ordinates.
  T get l0Norm => (x == 0 ? 0 : 1) + (y == 0 ? 0 : 1) as T;

  /// Returns the l-0 norm distance of this point to the other point.
  T l0NormDistanceTo(Point<T> other) => (this - other).l0Norm;

  /// Returns the l-1 norm metric of this point's co-ordinates.
  /// This is the magnitude of the co-ordinates.
  T get l1Norm {
    var absPoint = this.absolute;
    return absPoint.x + absPoint.y as T;
  }

  /// Returns the l-1 norm distance of this point to the other point.
  T l1NormDistanceTo(Point<T> other) => (this - other).l1Norm;

  /// Returns the l-2 norm metric of this point's co-ordinates.
  /// This is the magnitude of the co-ordinates.
  double get l2Norm => this.magnitude;

  /// Returns the l-2 norm distance of this point to the other point.
  double l2NormDistanceTo(Point<T> other) => (this - other).magnitude;

  /// Returns the l-infinity norm metric of this point's co-ordinates.
  T get lInfinityNorm {
    var absPoint = this.absolute;
    return max(absPoint.x as T, absPoint.y as T);
  }

  /// Returns the l-infinity norm distance of this point to the other point.
  T lInfinityNormDistanceTo(Point<T> other) => (this - other).lInfinityNorm;
}

extension PointDirectionalExt<T extends num> on Point<T> {
  static const DIRECTIONAL_UNIT_VECTOR_MAPPING =
      const <Direction_8_Way, Point<num>>{
    Direction_8_Way.NORTH: PointExt.UNIT_VECTOR_Y,
    Direction_8_Way.NORTH_EAST: PointExt.UNIT_VECTOR,
    Direction_8_Way.EAST: PointExt.UNIT_VECTOR_X,
    Direction_8_Way.SOUTH_EAST: const Point(1, -1),
    Direction_8_Way.SOUTH: const Point(0, -1),
    Direction_8_Way.SOUTH_WEST: const Point(-1, -1),
    Direction_8_Way.WEST: const Point(-1, 0),
    Direction_8_Way.NORTH_WEST: const Point(-1, 1),
  };

  Point<T> getNeighbour(Direction_8_Way inDirection) =>
      this + PointDirectionalExt.DIRECTIONAL_UNIT_VECTOR_MAPPING[inDirection];

  void forNeighbours(
      void function(Direction_8_Way direction, Point<T> neighbour)) {
    Direction_8_Way.values.forEach((direction) {
      function(
        direction,
        this + PointDirectionalExt.DIRECTIONAL_UNIT_VECTOR_MAPPING[direction],
      );
    });
  }
}

extension RectangleExt<T extends num> on Rectangle<T> {
  // Returns total points including points on edges.
  T get pointCount => (width + 1) * (height + 1) as T;
  T get area => width * height as T;

  /// Returns row major index of point in rectangle.
  /// Returns -1 if point not in rectangle.
  T getRowMajorIndexOfPoint(Point point) {
    if (!containsPoint(point)) return -1 as T;
    return (point.y - top) * (width + 1) + (point.x - left) as T;
  }

  /// Returns column major index of point in rectangle.
  /// Returns -1 if point not in rectangle.
  T getColumnMajorIndexOfPoint(Point point) {
    if (!containsPoint(point)) return -1 as T;
    return (point.x - left) * (height + 1) + (point.y - top) as T;
  }

  // Returns wrapped point at index if traversed row major.
  Point getRowMajorPointFromIndex(T index) {
    return Point(
      left + index % (width + 1),
      top + (index ~/ (height + 1)) % (height + 1),
    );
  }

  // Returns wrapped point at index if traversed column major.
  Point getColumnMajorPointFromIndex(T index) =>
      this.getRowMajorPointFromIndex(index).swapped;
}

extension GlobalKeyEx on GlobalKey {
  Rect get globalPaintBounds {
    final renderObject = currentContext?.findRenderObject();
    var translation = renderObject?.getTransformTo(null)?.getTranslation();
    if (translation != null && renderObject.paintBounds != null) {
      return renderObject.paintBounds
          .shift(Offset(translation.x, translation.y));
    } else {
      return null;
    }
  }
}
