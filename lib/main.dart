import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';
import 'package:tic_tac_toe/UI/in_game_page.dart';
import 'package:tic_tac_toe/UI/main_menu_page.dart';
import 'package:tic_tac_toe/blocs/theme/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:tic_tac_toe/services/AdMobService.dart';
import 'package:tic_tac_toe/services/AppService.dart';
import 'package:tic_tac_toe/services/GameService.dart';

MultiProvider _withRoot({@required Widget child}) => MultiProvider(
      providers: [
        BlocProvider<ThemeBloc>(
          create: (context) => ThemeBloc(),
        ),
        Provider<AdMobService>(
          lazy: false,
          create: (ctx) => AdMobService(
            appId: "ca-app-pub-2593141672113561~2989345713",
          ),
        ),
        Provider<AppService>(
          create: (ctx) => AppService(),
        )
      ],
      child: child,
    );

Widget _withThemeInjected({@required Widget child}) => _withRoot(
      child: BlocBuilder<ThemeBloc, ThemeState>(
        builder: (context, themeState) => MaterialApp(
          theme: themeState.lightTheme,
          darkTheme: themeState.darkTheme,
          themeMode: themeState.themeMode,
          home: Scaffold(
            body: SafeArea(
              child: child,
            ),
          ),
        ),
      ),
    );

Widget _app() {

  return _withThemeInjected(
    child: Observer(
      name: "Root Navigator",
      builder: (context) {
        var appService = Provider.of<AppService>(context);
        return WillPopScope(
          onWillPop: () async => appService.goBack() == false,
          child: AnimatedSwitcher(
            duration: Duration(milliseconds: 500),
            child: appService.navigationState.maybeWhen(
              main_menu: () => MainMenuPage(),
              in_game: (gameService) {
                return Provider<GameService>.value(
                  value: gameService,
                  child: InGamePage(),
                );
              },
              orElse: () => const Text("ERROR: Invalid Navigation."),
            ),
          ),
        );
      },
    ),
  );
}

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
    [
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ],
  ).then((_) => runApp(_app()));
}
