import 'dart:math';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:mobx/mobx.dart';
import 'package:tic_tac_toe/models/NavigationState.dart';
import 'package:tic_tac_toe/models/PlayerModel.dart';
import 'package:tic_tac_toe/models/TeamModel.dart';
import 'package:tic_tac_toe/repositories/BoardRepository.dart';
import 'package:tic_tac_toe/repositories/PlayersTeamsRepository.dart';
import 'package:tic_tac_toe/services/GameService.dart';

part 'AppService.g.dart';

class AppService = _AppService with _$AppService;

abstract class _AppService with Store {
  @observable
  NavigationState _navigationState = NavigationState.main_menu();

  void set navigationState(NavigationState value) {
    _navigationState.dispose();
    _navigationState = value;
  }

  @computed
  NavigationState get navigationState => _navigationState;

  /// Returns true if _navigationState changed to back page.
  /// Returns false if no previous page.
  @action
  bool goBack() {
    var backState = _navigationState.onBackState;
    if (backState.isEmpty) return false;
    navigationState = backState.value;
    return true;
  }

  @action
  void gotoMainMenu() {
    navigationState = NavigationState.main_menu();
  }

  @action
  void gotoSinglePlayerGame() {
    var teams = [
      TeamModel(
        teamImage: SvgPicture.asset("assets/X.svg"),
        teamTag: "X",
      ),
      TeamModel(
        teamImage: SvgPicture.asset("assets/O.svg"),
        teamTag: "O",
      ),
    ];
    var players = [
      LocalPlayer("Player 1"),
      LocalPlayer("Player 2"),
    ];
    navigationState = NavigationState.in_game(
      gameService: GameService(
        boardRepository: BoardRepository(
          boardRectangle: Rectangle(1, 1, 2, 2),
        ),
        playersTeamsRepository: PlayersTeamsRepository(
          teams: teams,
          players: players,
        )
          ..movePlayerToTeam(players[0], teams[0])
          ..movePlayerToTeam(players[1], teams[1]),
      ),
    );
  }
}
