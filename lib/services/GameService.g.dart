// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GameService.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$GameService on _GameService, Store {
  Computed<GameState> _$gameStateComputed;

  @override
  GameState get gameState =>
      (_$gameStateComputed ??= Computed<GameState>(() => super.gameState,
              name: '_GameService.gameState'))
          .value;

  final _$_gameStateAtom = Atom(name: '_GameService._gameState');

  @override
  GameState get _gameState {
    _$_gameStateAtom.reportRead();
    return super._gameState;
  }

  @override
  set _gameState(GameState value) {
    _$_gameStateAtom.reportWrite(value, super._gameState, () {
      super._gameState = value;
    });
  }

  final _$_GameServiceActionController = ActionController(name: '_GameService');

  @override
  bool doTurn(Point<num> boardPoint) {
    final _$actionInfo =
        _$_GameServiceActionController.startAction(name: '_GameService.doTurn');
    try {
      return super.doTurn(boardPoint);
    } finally {
      _$_GameServiceActionController.endAction(_$actionInfo);
    }
  }

  @override
  void resetGame({bool resetScores = false}) {
    final _$actionInfo = _$_GameServiceActionController.startAction(
        name: '_GameService.resetGame');
    try {
      return super.resetGame(resetScores: resetScores);
    } finally {
      _$_GameServiceActionController.endAction(_$actionInfo);
    }
  }

  @override
  Iterable<StrikeModel> _getLocalWinStrikes(
      {@required Point<num> boardPoint,
      @required TeamModel team,
      int winStrikeLength = 3}) {
    final _$actionInfo = _$_GameServiceActionController.startAction(
        name: '_GameService._getLocalWinStrikes');
    try {
      return super._getLocalWinStrikes(
          boardPoint: boardPoint, team: team, winStrikeLength: winStrikeLength);
    } finally {
      _$_GameServiceActionController.endAction(_$actionInfo);
    }
  }

  @override
  bool _checkLocalDrawCondition(Point<num> boardPoint) {
    final _$actionInfo = _$_GameServiceActionController.startAction(
        name: '_GameService._checkLocalDrawCondition');
    try {
      return super._checkLocalDrawCondition(boardPoint);
    } finally {
      _$_GameServiceActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
gameState: ${gameState}
    ''';
  }
}
