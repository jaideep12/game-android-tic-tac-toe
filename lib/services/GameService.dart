import 'dart:collection';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mobx/mobx.dart';
import 'package:tic_tac_toe/math_helper.dart';
import 'package:tic_tac_toe/models/GameState.dart';
import 'package:tic_tac_toe/models/StrikeModel.dart';
import 'package:tic_tac_toe/models/TeamModel.dart';
import 'package:tic_tac_toe/repositories/BoardRepository.dart';
import 'package:tic_tac_toe/repositories/PlayersTeamsRepository.dart';
import 'package:tic_tac_toe/repositories/ScoreRepository.dart';
import 'package:tic_tac_toe/services/PlayerTurnService.dart';

part 'GameService.g.dart';

class GameService = _GameService with _$GameService;

abstract class _GameService with Store {
  final BoardRepository boardRepository;
  final PlayersTeamsRepository playersTeamsRepository;
  final ScoreRepository<TeamModel, int> scoreRepository;
  final PlayerTurnService playerTurnService;

  @observable
  GameState _gameState;
  @computed
  GameState get gameState => _gameState;

  _GameService({
    @required this.boardRepository,
    @required this.playersTeamsRepository,
  })  : _gameState = const PreGame(),
        playerTurnService = PlayerTurnService(playersTeamsRepository),
        scoreRepository = ScoreRepository(
          initialScore: 0,
          entities: playersTeamsRepository.teams,
        );

  /// Performs current player's turn at the given board point.
  /// Returns true if current player turn successfully done, else returns false.
  @action
  bool doTurn(Point boardPoint) {
    var turnPlayer = playerTurnService.currentTurnPlayer;
    var turnTeam = playerTurnService.currentTurnTeam;
    // check if current player can do input
    if (!turnPlayer.canGiveInput) return false;
    // return false if board couldn't be updated
    if (!boardRepository.setTeamAt(boardPoint, turnTeam)) return false;
    playerTurnService.finishTeamTurn();
    // update game state
    Iterable<StrikeModel> winStrikes = _getLocalWinStrikes(
      boardPoint: boardPoint,
      team: turnTeam,
    );
    if (winStrikes.isNotEmpty) {
      // this team won the game
      scoreRepository.setScore(
          turnTeam, (scoreRepository.getScore(turnTeam).orElse(0) as int) + 1);
      _gameState = GameState.game_ended(
        winners: UnmodifiableListView([turnTeam]),
        winStrikes: UnmodifiableListView(winStrikes),
      );
    } else if (_checkLocalDrawCondition(boardPoint)) {
      _gameState = GameState.game_ended(
        winners: UnmodifiableListView(playersTeamsRepository.teams),
        winStrikes: UnmodifiableListView(const []),
      );
    }
    return true;
  }

  @action
  void resetGame({bool resetScores = false}) {
    if (resetScores) scoreRepository.resetAllScores();
    boardRepository.clearBoard();
    playerTurnService.resetPlayerTurns();
    _gameState = GameState.in_game();
  }

  /// Returns the strikes for the given team around the given point.
  Iterable<StrikeModel> _getLocalStrikes({
    @required Point boardPoint,
    @required TeamModel team,
  }) {
    if (boardRepository.getTeamAt(boardPoint).orElse(null) != team)
      return const [];
    List<StrikeModel> retVal = [];
    for (var direction in const [
      Direction_8_Way.NORTH,
      Direction_8_Way.WEST,
      Direction_8_Way.NORTH_WEST,
      Direction_8_Way.NORTH_EAST,
    ]) {
      int len = 0;
      // 1st direction
      var vector =
          PointDirectionalExt.DIRECTIONAL_UNIT_VECTOR_MAPPING[direction];
      var cursor_1 = boardPoint + vector;
      while (boardRepository.getTeamAt(cursor_1).contains(team)) {
        cursor_1 += vector;
        ++len;
      }
      cursor_1 -= vector;
      // opposite direction
      vector = PointDirectionalExt
          .DIRECTIONAL_UNIT_VECTOR_MAPPING[direction.opposite];
      var cursor_2 = boardPoint + vector;
      while (boardRepository.getTeamAt(cursor_2).contains(team)) {
        cursor_2 += vector;
        ++len;
      }
      cursor_2 -= vector;
      if (cursor_1 != cursor_2)
        retVal.add(StrikeModel.lineStrike(
          start: cursor_1,
          direction: direction.opposite,
          length: len + 1,
        ));
    }
    if (retVal.isEmpty) retVal.add(StrikeModel.pointStrike(start: boardPoint));
    return retVal;
  }

  /// Checks for the win condition around the given point.
  /// Returns true if game won.
  @action
  Iterable<StrikeModel> _getLocalWinStrikes({
    @required Point boardPoint,
    @required TeamModel team,
    int winStrikeLength = 3,
  }) {
    return _getLocalStrikes(boardPoint: boardPoint, team: team)
        .where((strike) => strike.strikeLength >= winStrikeLength);
  }

  /// Checks for the win condition around the given point.
  /// Returns true if game won.
  @action
  bool _checkLocalDrawCondition(Point boardPoint) {
    return !boardRepository.grid
        .any((boardTeamOptional) => boardTeamOptional.isEmpty);
  }

  void dispose() {
    playerTurnService.dispose();
  }
}
