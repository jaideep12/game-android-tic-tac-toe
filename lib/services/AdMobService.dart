import 'package:auto_size_text/auto_size_text.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_admob/flutter_native_admob.dart';
import 'package:flutter_native_admob/native_admob_options.dart';
import 'package:mobx/mobx.dart';
import 'package:tic_tac_toe/UI/widgets/AdMob/banner_ad.dart';

part 'AdMobService.g.dart';

class AdMobService = _AdMobService with _$AdMobService;

enum AdMobUnit {
  Banner,
  Interstitial,
  Interstitial_Video,
  Rewarded_Video,
  Native_Advanced,
  Native_Advanced_Video,
}

const AD_TEST_UNITS = const {
  AdMobUnit.Banner: "ca-app-pub-3940256099942544/6300978111",
  AdMobUnit.Interstitial: "ca-app-pub-3940256099942544/1033173712",
  AdMobUnit.Interstitial_Video: "ca-app-pub-3940256099942544/8691691433",
  AdMobUnit.Rewarded_Video: "ca-app-pub-3940256099942544/5224354917",
  AdMobUnit.Native_Advanced: "ca-app-pub-3940256099942544/2247696110",
  AdMobUnit.Native_Advanced_Video: "ca-app-pub-3940256099942544/1044960115",
};

const AD_RELEASE_UNITS = {
  "main_menu": const [
    const {
      "type": AdMobUnit.Banner,
      "id": "ca-app-pub-2593141672113561/3442573607",
    },
    const {
      "type": AdMobUnit.Banner,
      "id": "ca-app-pub-2593141672113561/8622085388",
    },
  ],
  "in_game": const [
    const {
      "type": AdMobUnit.Native_Advanced,
      "id": "ca-app-pub-2593141672113561/3160143201",
    },
  ],
};

abstract class _AdMobService with Store {
  BannerAd ad;

  _AdMobService({
    @required String appId,
    String trackingId,
    bool analyticsEnabled = false,
  }) {
    FirebaseAdMob.instance.initialize(
      appId: appId,
      trackingId: trackingId,
      analyticsEnabled: analyticsEnabled,
    );
  }

  Widget adWidgetMainMenu(BuildContext context, int index) {
    return Container(
      child: BannerAdWidget(
        adUnitId: AD_RELEASE_UNITS["main_menu"][index]["id"],
        size: AdSize.smartBanner,
        error: const AutoSizeText("Error couldn't load ad."),
        loading: Center(
          child: const CircularProgressIndicator(),
        ),
      ),
    );
  }

  Widget adWidgetInGame(BuildContext context, int index) {
    return NativeAdmob(
      adUnitID: AD_RELEASE_UNITS["in_game"][index]["id"],
      loading: const CircularProgressIndicator(),
      error: const AutoSizeText("Error couldn't load ad."),
      type: NativeAdmobType.banner,
      options: NativeAdmobOptions(
        storeTextStyle: NativeTextStyle(
          color: Theme.of(context).highlightColor,
          backgroundColor: Theme.of(context).backgroundColor,
        ),
        adLabelTextStyle: NativeTextStyle(
          backgroundColor: Colors.green,
        ),
        advertiserTextStyle: NativeTextStyle(
          color: Theme.of(context).textSelectionColor,
        ),
        headlineTextStyle: NativeTextStyle(
          color: Theme.of(context).textSelectionColor,
        ),
      ),
    );
  }
}
