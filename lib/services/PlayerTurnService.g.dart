// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'PlayerTurnService.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PlayerTurnService on _PlayerTurnService, Store {
  Computed<TeamModel> _$currentTurnTeamComputed;

  @override
  TeamModel get currentTurnTeam => (_$currentTurnTeamComputed ??=
          Computed<TeamModel>(() => super.currentTurnTeam,
              name: '_PlayerTurnService.currentTurnTeam'))
      .value;
  Computed<PlayerModel> _$currentTurnPlayerComputed;

  @override
  PlayerModel get currentTurnPlayer => (_$currentTurnPlayerComputed ??=
          Computed<PlayerModel>(() => super.currentTurnPlayer,
              name: '_PlayerTurnService.currentTurnPlayer'))
      .value;

  final _$_currentTurnTeamIndexAtom =
      Atom(name: '_PlayerTurnService._currentTurnTeamIndex');

  @override
  int get _currentTurnTeamIndex {
    _$_currentTurnTeamIndexAtom.reportRead();
    return super._currentTurnTeamIndex;
  }

  @override
  set _currentTurnTeamIndex(int value) {
    _$_currentTurnTeamIndexAtom.reportWrite(value, super._currentTurnTeamIndex,
        () {
      super._currentTurnTeamIndex = value;
    });
  }

  final _$_PlayerTurnServiceActionController =
      ActionController(name: '_PlayerTurnService');

  @override
  void finishTeamTurn() {
    final _$actionInfo = _$_PlayerTurnServiceActionController.startAction(
        name: '_PlayerTurnService.finishTeamTurn');
    try {
      return super.finishTeamTurn();
    } finally {
      _$_PlayerTurnServiceActionController.endAction(_$actionInfo);
    }
  }

  @override
  void resetPlayerTurns() {
    final _$actionInfo = _$_PlayerTurnServiceActionController.startAction(
        name: '_PlayerTurnService.resetPlayerTurns');
    try {
      return super.resetPlayerTurns();
    } finally {
      _$_PlayerTurnServiceActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
currentTurnTeam: ${currentTurnTeam},
currentTurnPlayer: ${currentTurnPlayer}
    ''';
  }
}
