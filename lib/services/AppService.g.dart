// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AppService.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AppService on _AppService, Store {
  Computed<NavigationState> _$navigationStateComputed;

  @override
  NavigationState get navigationState => (_$navigationStateComputed ??=
          Computed<NavigationState>(() => super.navigationState,
              name: '_AppService.navigationState'))
      .value;

  final _$_navigationStateAtom = Atom(name: '_AppService._navigationState');

  @override
  NavigationState get _navigationState {
    _$_navigationStateAtom.reportRead();
    return super._navigationState;
  }

  @override
  set _navigationState(NavigationState value) {
    _$_navigationStateAtom.reportWrite(value, super._navigationState, () {
      super._navigationState = value;
    });
  }

  final _$_AppServiceActionController = ActionController(name: '_AppService');

  @override
  bool goBack() {
    final _$actionInfo =
        _$_AppServiceActionController.startAction(name: '_AppService.goBack');
    try {
      return super.goBack();
    } finally {
      _$_AppServiceActionController.endAction(_$actionInfo);
    }
  }

  @override
  void gotoMainMenu() {
    final _$actionInfo = _$_AppServiceActionController.startAction(
        name: '_AppService.gotoMainMenu');
    try {
      return super.gotoMainMenu();
    } finally {
      _$_AppServiceActionController.endAction(_$actionInfo);
    }
  }

  @override
  void gotoSinglePlayerGame() {
    final _$actionInfo = _$_AppServiceActionController.startAction(
        name: '_AppService.gotoSinglePlayerGame');
    try {
      return super.gotoSinglePlayerGame();
    } finally {
      _$_AppServiceActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
navigationState: ${navigationState}
    ''';
  }
}
