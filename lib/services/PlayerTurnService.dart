import 'package:mobx/mobx.dart';
import 'package:tic_tac_toe/models/PlayerModel.dart';
import 'package:tic_tac_toe/models/TeamModel.dart';
import 'package:tic_tac_toe/repositories/PlayersTeamsRepository.dart';

part 'PlayerTurnService.g.dart';

class PlayerTurnService = _PlayerTurnService with _$PlayerTurnService;

abstract class _PlayerTurnService with Store {
  final PlayersTeamsRepository _playersTeamsRepository;
  final ObservableMap<TeamModel, int> _teamPlayerTurnIndexMapping;
  ReactionDisposer _repoSyncDisposer;

  /// This is an observable to allow its dependent computed values to update their cache.
  @observable
  int _currentTurnTeamIndex;

  _PlayerTurnService(this._playersTeamsRepository)
      : _teamPlayerTurnIndexMapping = ObservableMap(),
        _currentTurnTeamIndex = 0 {
    // copy data already in _playersTeamsRepository
    _teamPlayerTurnIndexMapping.addEntries(
        _playersTeamsRepository.teams.map((team) => MapEntry(team, 0)));
    // setup auto sync to _playersTeamsRepository
    _repoSyncDisposer = reaction(
      (reaction) => _playersTeamsRepository.changeEvent,
      (PlayersTeamsRepositoryEvent changeEvent) {
        if (changeEvent is TeamChangeEvent) {
          _currentTurnTeamIndex = _clipWrap(
            _currentTurnTeamIndex,
            lowerLimitInc: 0,
            upperLimitInc: _playersTeamsRepository.teams.length - 1,
          );
        } else if (changeEvent is PlayerChangeEvent) {
          var teams = [
            changeEvent.sourceTeam,
            changeEvent.targetTeam,
          ];
          for (var team in teams)
            if (team != null) {
              int len =
                  _playersTeamsRepository.getTeamPlayerList(team).value.length;
              _teamPlayerTurnIndexMapping[team] = _clipWrap(
                _teamPlayerTurnIndexMapping[team],
                lowerLimitInc: 0,
                upperLimitInc: len - 1,
              );
            }
        }
      },
      name: "onPlayersTeamsRepoChange",
    );
  }

  @computed
  TeamModel get currentTurnTeam =>
      _playersTeamsRepository.teams.elementAt(_currentTurnTeamIndex);

  @computed
  PlayerModel get currentTurnPlayer => _playersTeamsRepository
      .getTeamPlayerList(currentTurnTeam)
      .value
      .elementAt(_teamPlayerTurnIndexMapping[currentTurnTeam]);

  @action
  void finishTeamTurn() {
    // increment player idx
    _teamPlayerTurnIndexMapping[currentTurnTeam] = _clipWrap(
      _teamPlayerTurnIndexMapping[currentTurnTeam] + 1,
      lowerLimitInc: 0,
      upperLimitInc:
          _playersTeamsRepository.getTeamPlayerList(currentTurnTeam).length - 1,
    );
    // increment team idx
    _currentTurnTeamIndex = _clipWrap(
      _currentTurnTeamIndex + 1,
      lowerLimitInc: 0,
      upperLimitInc: _playersTeamsRepository.teams.length - 1,
    );
  }

  /// Resets turn pointers to the way they are at start of the game.
  @action
  void resetPlayerTurns() {
    _currentTurnTeamIndex = 0;
    _teamPlayerTurnIndexMapping.updateAll((team, playerIdx) => 0);
  }

  int _clipWrap(int value, {int lowerLimitInc = 0, int upperLimitInc = 10}) {
    if (value < lowerLimitInc) return upperLimitInc;
    if (value > upperLimitInc) return lowerLimitInc;
    return value;
  }

  void dispose() {
    _repoSyncDisposer();
  }
}
