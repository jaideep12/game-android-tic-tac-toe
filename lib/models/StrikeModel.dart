import 'dart:math';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:optional/optional.dart';

import '../math_helper.dart';

part 'StrikeModel.freezed.dart';

@freezed
abstract class StrikeModel implements _$StrikeModel {
  const StrikeModel._(); // Added constructor
  const factory StrikeModel.lineStrike({
    @required Point start,
    @required Direction_8_Way direction,
    // Length is total iterable points
    @required int length,
  }) = LineStrike;
  const factory StrikeModel.pointStrike({
    @required Point start,
  }) = PointStrike;

  Optional<StrikeModel> extended(Direction_8_Way direction) {
    var me = this;
    if (me is LineStrike && me.direction == direction) {
      return me.copyWith(length: me.length + 1).toOptional;
    } else if (me is PointStrike) {
      return StrikeModel.lineStrike(
        start: me.start,
        direction: direction,
        length: 2,
      ).toOptional;
    } else
      return const Optional.empty();
  }

  Point get startPoint => start;
  Point get endPoint => this.when(
        lineStrike: (point, dir, len) =>
            start +
            PointDirectionalExt.DIRECTIONAL_UNIT_VECTOR_MAPPING[dir]
                .scaled(len - 1),
        pointStrike: (point) => point,
      );

  Iterable<Point> get strikePoints {
    return this.when(
      pointStrike: (start) => Iterable.generate(1, (idx) => start),
      lineStrike: (start, direction, length) => Iterable.generate(
        length,
        (idx) =>
            start +
            PointDirectionalExt.DIRECTIONAL_UNIT_VECTOR_MAPPING[direction]
                .scaled(idx),
      ),
    );
  }

  int get strikeLength => this.when(
        lineStrike: (point, dir, len) => len,
        pointStrike: (point) => 1,
      );
}
