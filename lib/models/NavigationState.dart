import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:optional/optional.dart';
import 'package:tic_tac_toe/services/GameService.dart';

part 'NavigationState.freezed.dart';

@freezed
abstract class NavigationState implements _$NavigationState {
  const NavigationState._();
  const factory NavigationState.main_menu() = MainMenu;
  const factory NavigationState.in_game({
    @required GameService gameService,
  }) = InGame;

  Optional<NavigationState> get onBackState {
    if (this is MainMenu)
      return const Optional.empty();
    else if (this is InGame)
      return NavigationState.main_menu().toOptional;
    else
      return const Optional.empty();
  }

  void dispose() {
    if (this is InGame) (this as InGame).gameService.dispose();
  }
}
