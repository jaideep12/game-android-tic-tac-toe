import 'package:tic_tac_toe/models/Entity.dart';
import 'package:flutter/foundation.dart';

abstract class PlayerModel extends Entity {
  PlayerModel({@required String playerName}) : super(name: playerName);
  String get playerName => name;
  bool get canGiveInput;
}

class LocalPlayer extends PlayerModel {
  LocalPlayer(String playerName) : super(playerName: playerName);
  @override
  bool get canGiveInput => true;
}

class NetworkPlayer extends PlayerModel {
  NetworkPlayer(String playerName) : super(playerName: playerName);
  @override
  bool get canGiveInput => false;
}
