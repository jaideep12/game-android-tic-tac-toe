import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class Entity extends Equatable {
  final String name;

  Entity({@required this.name});

  @override
  List<Object> get props => [
        name,
      ];
}