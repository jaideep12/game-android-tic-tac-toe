import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:tic_tac_toe/models/StrikeModel.dart';
import 'TeamModel.dart';

part 'GameState.freezed.dart';

@freezed
abstract class GameState implements _$GameState {
  const GameState._(); // Added constructor
  const factory GameState.pre_game() = PreGame;
  const factory GameState.in_game() = InGame;
  const factory GameState.game_ended({
    @required UnmodifiableListView<TeamModel> winners,
    @required UnmodifiableListView<StrikeModel> winStrikes,
  }) = GameEnded;
}
