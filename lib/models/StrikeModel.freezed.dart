// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'StrikeModel.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$StrikeModelTearOff {
  const _$StrikeModelTearOff();

  LineStrike lineStrike(
      {@required Point<num> start,
      @required Direction_8_Way direction,
      @required int length}) {
    return LineStrike(
      start: start,
      direction: direction,
      length: length,
    );
  }

  PointStrike pointStrike({@required Point<num> start}) {
    return PointStrike(
      start: start,
    );
  }
}

// ignore: unused_element
const $StrikeModel = _$StrikeModelTearOff();

mixin _$StrikeModel {
  Point<num> get start;

  @optionalTypeArgs
  Result when<Result extends Object>({
    @required
        Result lineStrike(
            Point<num> start, Direction_8_Way direction, int length),
    @required Result pointStrike(Point<num> start),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result lineStrike(Point<num> start, Direction_8_Way direction, int length),
    Result pointStrike(Point<num> start),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result lineStrike(LineStrike value),
    @required Result pointStrike(PointStrike value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result lineStrike(LineStrike value),
    Result pointStrike(PointStrike value),
    @required Result orElse(),
  });

  $StrikeModelCopyWith<StrikeModel> get copyWith;
}

abstract class $StrikeModelCopyWith<$Res> {
  factory $StrikeModelCopyWith(
          StrikeModel value, $Res Function(StrikeModel) then) =
      _$StrikeModelCopyWithImpl<$Res>;
  $Res call({Point<num> start});
}

class _$StrikeModelCopyWithImpl<$Res> implements $StrikeModelCopyWith<$Res> {
  _$StrikeModelCopyWithImpl(this._value, this._then);

  final StrikeModel _value;
  // ignore: unused_field
  final $Res Function(StrikeModel) _then;

  @override
  $Res call({
    Object start = freezed,
  }) {
    return _then(_value.copyWith(
      start: start == freezed ? _value.start : start as Point<num>,
    ));
  }
}

abstract class $LineStrikeCopyWith<$Res> implements $StrikeModelCopyWith<$Res> {
  factory $LineStrikeCopyWith(
          LineStrike value, $Res Function(LineStrike) then) =
      _$LineStrikeCopyWithImpl<$Res>;
  @override
  $Res call({Point<num> start, Direction_8_Way direction, int length});
}

class _$LineStrikeCopyWithImpl<$Res> extends _$StrikeModelCopyWithImpl<$Res>
    implements $LineStrikeCopyWith<$Res> {
  _$LineStrikeCopyWithImpl(LineStrike _value, $Res Function(LineStrike) _then)
      : super(_value, (v) => _then(v as LineStrike));

  @override
  LineStrike get _value => super._value as LineStrike;

  @override
  $Res call({
    Object start = freezed,
    Object direction = freezed,
    Object length = freezed,
  }) {
    return _then(LineStrike(
      start: start == freezed ? _value.start : start as Point<num>,
      direction: direction == freezed
          ? _value.direction
          : direction as Direction_8_Way,
      length: length == freezed ? _value.length : length as int,
    ));
  }
}

class _$LineStrike extends LineStrike {
  const _$LineStrike(
      {@required this.start, @required this.direction, @required this.length})
      : assert(start != null),
        assert(direction != null),
        assert(length != null),
        super._();

  @override
  final Point<num> start;
  @override
  final Direction_8_Way direction;
  @override
  final int length;

  @override
  String toString() {
    return 'StrikeModel.lineStrike(start: $start, direction: $direction, length: $length)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LineStrike &&
            (identical(other.start, start) ||
                const DeepCollectionEquality().equals(other.start, start)) &&
            (identical(other.direction, direction) ||
                const DeepCollectionEquality()
                    .equals(other.direction, direction)) &&
            (identical(other.length, length) ||
                const DeepCollectionEquality().equals(other.length, length)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(start) ^
      const DeepCollectionEquality().hash(direction) ^
      const DeepCollectionEquality().hash(length);

  @override
  $LineStrikeCopyWith<LineStrike> get copyWith =>
      _$LineStrikeCopyWithImpl<LineStrike>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required
        Result lineStrike(
            Point<num> start, Direction_8_Way direction, int length),
    @required Result pointStrike(Point<num> start),
  }) {
    assert(lineStrike != null);
    assert(pointStrike != null);
    return lineStrike(start, direction, length);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result lineStrike(Point<num> start, Direction_8_Way direction, int length),
    Result pointStrike(Point<num> start),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (lineStrike != null) {
      return lineStrike(start, direction, length);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result lineStrike(LineStrike value),
    @required Result pointStrike(PointStrike value),
  }) {
    assert(lineStrike != null);
    assert(pointStrike != null);
    return lineStrike(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result lineStrike(LineStrike value),
    Result pointStrike(PointStrike value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (lineStrike != null) {
      return lineStrike(this);
    }
    return orElse();
  }
}

abstract class LineStrike extends StrikeModel {
  const LineStrike._() : super._();
  const factory LineStrike(
      {@required Point<num> start,
      @required Direction_8_Way direction,
      @required int length}) = _$LineStrike;

  @override
  Point<num> get start;
  Direction_8_Way get direction;
  int get length;
  @override
  $LineStrikeCopyWith<LineStrike> get copyWith;
}

abstract class $PointStrikeCopyWith<$Res>
    implements $StrikeModelCopyWith<$Res> {
  factory $PointStrikeCopyWith(
          PointStrike value, $Res Function(PointStrike) then) =
      _$PointStrikeCopyWithImpl<$Res>;
  @override
  $Res call({Point<num> start});
}

class _$PointStrikeCopyWithImpl<$Res> extends _$StrikeModelCopyWithImpl<$Res>
    implements $PointStrikeCopyWith<$Res> {
  _$PointStrikeCopyWithImpl(
      PointStrike _value, $Res Function(PointStrike) _then)
      : super(_value, (v) => _then(v as PointStrike));

  @override
  PointStrike get _value => super._value as PointStrike;

  @override
  $Res call({
    Object start = freezed,
  }) {
    return _then(PointStrike(
      start: start == freezed ? _value.start : start as Point<num>,
    ));
  }
}

class _$PointStrike extends PointStrike {
  const _$PointStrike({@required this.start})
      : assert(start != null),
        super._();

  @override
  final Point<num> start;

  @override
  String toString() {
    return 'StrikeModel.pointStrike(start: $start)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is PointStrike &&
            (identical(other.start, start) ||
                const DeepCollectionEquality().equals(other.start, start)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(start);

  @override
  $PointStrikeCopyWith<PointStrike> get copyWith =>
      _$PointStrikeCopyWithImpl<PointStrike>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required
        Result lineStrike(
            Point<num> start, Direction_8_Way direction, int length),
    @required Result pointStrike(Point<num> start),
  }) {
    assert(lineStrike != null);
    assert(pointStrike != null);
    return pointStrike(start);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result lineStrike(Point<num> start, Direction_8_Way direction, int length),
    Result pointStrike(Point<num> start),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (pointStrike != null) {
      return pointStrike(start);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result lineStrike(LineStrike value),
    @required Result pointStrike(PointStrike value),
  }) {
    assert(lineStrike != null);
    assert(pointStrike != null);
    return pointStrike(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result lineStrike(LineStrike value),
    Result pointStrike(PointStrike value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (pointStrike != null) {
      return pointStrike(this);
    }
    return orElse();
  }
}

abstract class PointStrike extends StrikeModel {
  const PointStrike._() : super._();
  const factory PointStrike({@required Point<num> start}) = _$PointStrike;

  @override
  Point<num> get start;
  @override
  $PointStrikeCopyWith<PointStrike> get copyWith;
}
