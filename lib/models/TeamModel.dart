import 'package:flutter_svg/flutter_svg.dart';
import 'package:tic_tac_toe/models/Entity.dart';
import 'package:flutter/foundation.dart';

class TeamModel extends Entity with Comparable<TeamModel> {
  final String teamTag;
  final SvgPicture teamImage;

  /// Asserts teamTag != null.
  TeamModel({
    @required this.teamTag,
    @required this.teamImage,
  }) : super(name: teamTag);

  @override
  int compareTo(TeamModel other) {
    if (this == other)
      return 0;
    else
      return teamTag.compareTo(other.teamTag);
  }
}
