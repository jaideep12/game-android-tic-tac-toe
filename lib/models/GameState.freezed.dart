// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'GameState.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$GameStateTearOff {
  const _$GameStateTearOff();

  PreGame pre_game() {
    return const PreGame();
  }

  InGame in_game() {
    return const InGame();
  }

  GameEnded game_ended(
      {@required UnmodifiableListView<TeamModel> winners,
      @required UnmodifiableListView<StrikeModel> winStrikes}) {
    return GameEnded(
      winners: winners,
      winStrikes: winStrikes,
    );
  }
}

// ignore: unused_element
const $GameState = _$GameStateTearOff();

mixin _$GameState {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result pre_game(),
    @required Result in_game(),
    @required
        Result game_ended(UnmodifiableListView<TeamModel> winners,
            UnmodifiableListView<StrikeModel> winStrikes),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result pre_game(),
    Result in_game(),
    Result game_ended(UnmodifiableListView<TeamModel> winners,
        UnmodifiableListView<StrikeModel> winStrikes),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result pre_game(PreGame value),
    @required Result in_game(InGame value),
    @required Result game_ended(GameEnded value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result pre_game(PreGame value),
    Result in_game(InGame value),
    Result game_ended(GameEnded value),
    @required Result orElse(),
  });
}

abstract class $GameStateCopyWith<$Res> {
  factory $GameStateCopyWith(GameState value, $Res Function(GameState) then) =
      _$GameStateCopyWithImpl<$Res>;
}

class _$GameStateCopyWithImpl<$Res> implements $GameStateCopyWith<$Res> {
  _$GameStateCopyWithImpl(this._value, this._then);

  final GameState _value;
  // ignore: unused_field
  final $Res Function(GameState) _then;
}

abstract class $PreGameCopyWith<$Res> {
  factory $PreGameCopyWith(PreGame value, $Res Function(PreGame) then) =
      _$PreGameCopyWithImpl<$Res>;
}

class _$PreGameCopyWithImpl<$Res> extends _$GameStateCopyWithImpl<$Res>
    implements $PreGameCopyWith<$Res> {
  _$PreGameCopyWithImpl(PreGame _value, $Res Function(PreGame) _then)
      : super(_value, (v) => _then(v as PreGame));

  @override
  PreGame get _value => super._value as PreGame;
}

class _$PreGame extends PreGame {
  const _$PreGame() : super._();

  @override
  String toString() {
    return 'GameState.pre_game()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is PreGame);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result pre_game(),
    @required Result in_game(),
    @required
        Result game_ended(UnmodifiableListView<TeamModel> winners,
            UnmodifiableListView<StrikeModel> winStrikes),
  }) {
    assert(pre_game != null);
    assert(in_game != null);
    assert(game_ended != null);
    return pre_game();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result pre_game(),
    Result in_game(),
    Result game_ended(UnmodifiableListView<TeamModel> winners,
        UnmodifiableListView<StrikeModel> winStrikes),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (pre_game != null) {
      return pre_game();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result pre_game(PreGame value),
    @required Result in_game(InGame value),
    @required Result game_ended(GameEnded value),
  }) {
    assert(pre_game != null);
    assert(in_game != null);
    assert(game_ended != null);
    return pre_game(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result pre_game(PreGame value),
    Result in_game(InGame value),
    Result game_ended(GameEnded value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (pre_game != null) {
      return pre_game(this);
    }
    return orElse();
  }
}

abstract class PreGame extends GameState {
  const PreGame._() : super._();
  const factory PreGame() = _$PreGame;
}

abstract class $InGameCopyWith<$Res> {
  factory $InGameCopyWith(InGame value, $Res Function(InGame) then) =
      _$InGameCopyWithImpl<$Res>;
}

class _$InGameCopyWithImpl<$Res> extends _$GameStateCopyWithImpl<$Res>
    implements $InGameCopyWith<$Res> {
  _$InGameCopyWithImpl(InGame _value, $Res Function(InGame) _then)
      : super(_value, (v) => _then(v as InGame));

  @override
  InGame get _value => super._value as InGame;
}

class _$InGame extends InGame {
  const _$InGame() : super._();

  @override
  String toString() {
    return 'GameState.in_game()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is InGame);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result pre_game(),
    @required Result in_game(),
    @required
        Result game_ended(UnmodifiableListView<TeamModel> winners,
            UnmodifiableListView<StrikeModel> winStrikes),
  }) {
    assert(pre_game != null);
    assert(in_game != null);
    assert(game_ended != null);
    return in_game();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result pre_game(),
    Result in_game(),
    Result game_ended(UnmodifiableListView<TeamModel> winners,
        UnmodifiableListView<StrikeModel> winStrikes),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (in_game != null) {
      return in_game();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result pre_game(PreGame value),
    @required Result in_game(InGame value),
    @required Result game_ended(GameEnded value),
  }) {
    assert(pre_game != null);
    assert(in_game != null);
    assert(game_ended != null);
    return in_game(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result pre_game(PreGame value),
    Result in_game(InGame value),
    Result game_ended(GameEnded value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (in_game != null) {
      return in_game(this);
    }
    return orElse();
  }
}

abstract class InGame extends GameState {
  const InGame._() : super._();
  const factory InGame() = _$InGame;
}

abstract class $GameEndedCopyWith<$Res> {
  factory $GameEndedCopyWith(GameEnded value, $Res Function(GameEnded) then) =
      _$GameEndedCopyWithImpl<$Res>;
  $Res call(
      {UnmodifiableListView<TeamModel> winners,
      UnmodifiableListView<StrikeModel> winStrikes});
}

class _$GameEndedCopyWithImpl<$Res> extends _$GameStateCopyWithImpl<$Res>
    implements $GameEndedCopyWith<$Res> {
  _$GameEndedCopyWithImpl(GameEnded _value, $Res Function(GameEnded) _then)
      : super(_value, (v) => _then(v as GameEnded));

  @override
  GameEnded get _value => super._value as GameEnded;

  @override
  $Res call({
    Object winners = freezed,
    Object winStrikes = freezed,
  }) {
    return _then(GameEnded(
      winners: winners == freezed
          ? _value.winners
          : winners as UnmodifiableListView<TeamModel>,
      winStrikes: winStrikes == freezed
          ? _value.winStrikes
          : winStrikes as UnmodifiableListView<StrikeModel>,
    ));
  }
}

class _$GameEnded extends GameEnded {
  const _$GameEnded({@required this.winners, @required this.winStrikes})
      : assert(winners != null),
        assert(winStrikes != null),
        super._();

  @override
  final UnmodifiableListView<TeamModel> winners;
  @override
  final UnmodifiableListView<StrikeModel> winStrikes;

  @override
  String toString() {
    return 'GameState.game_ended(winners: $winners, winStrikes: $winStrikes)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is GameEnded &&
            (identical(other.winners, winners) ||
                const DeepCollectionEquality()
                    .equals(other.winners, winners)) &&
            (identical(other.winStrikes, winStrikes) ||
                const DeepCollectionEquality()
                    .equals(other.winStrikes, winStrikes)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(winners) ^
      const DeepCollectionEquality().hash(winStrikes);

  @override
  $GameEndedCopyWith<GameEnded> get copyWith =>
      _$GameEndedCopyWithImpl<GameEnded>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result pre_game(),
    @required Result in_game(),
    @required
        Result game_ended(UnmodifiableListView<TeamModel> winners,
            UnmodifiableListView<StrikeModel> winStrikes),
  }) {
    assert(pre_game != null);
    assert(in_game != null);
    assert(game_ended != null);
    return game_ended(winners, winStrikes);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result pre_game(),
    Result in_game(),
    Result game_ended(UnmodifiableListView<TeamModel> winners,
        UnmodifiableListView<StrikeModel> winStrikes),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (game_ended != null) {
      return game_ended(winners, winStrikes);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result pre_game(PreGame value),
    @required Result in_game(InGame value),
    @required Result game_ended(GameEnded value),
  }) {
    assert(pre_game != null);
    assert(in_game != null);
    assert(game_ended != null);
    return game_ended(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result pre_game(PreGame value),
    Result in_game(InGame value),
    Result game_ended(GameEnded value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (game_ended != null) {
      return game_ended(this);
    }
    return orElse();
  }
}

abstract class GameEnded extends GameState {
  const GameEnded._() : super._();
  const factory GameEnded(
      {@required UnmodifiableListView<TeamModel> winners,
      @required UnmodifiableListView<StrikeModel> winStrikes}) = _$GameEnded;

  UnmodifiableListView<TeamModel> get winners;
  UnmodifiableListView<StrikeModel> get winStrikes;
  $GameEndedCopyWith<GameEnded> get copyWith;
}
