// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'NavigationState.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$NavigationStateTearOff {
  const _$NavigationStateTearOff();

  MainMenu main_menu() {
    return const MainMenu();
  }

  InGame in_game({@required GameService gameService}) {
    return InGame(
      gameService: gameService,
    );
  }
}

// ignore: unused_element
const $NavigationState = _$NavigationStateTearOff();

mixin _$NavigationState {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result main_menu(),
    @required Result in_game(GameService gameService),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result main_menu(),
    Result in_game(GameService gameService),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result main_menu(MainMenu value),
    @required Result in_game(InGame value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result main_menu(MainMenu value),
    Result in_game(InGame value),
    @required Result orElse(),
  });
}

abstract class $NavigationStateCopyWith<$Res> {
  factory $NavigationStateCopyWith(
          NavigationState value, $Res Function(NavigationState) then) =
      _$NavigationStateCopyWithImpl<$Res>;
}

class _$NavigationStateCopyWithImpl<$Res>
    implements $NavigationStateCopyWith<$Res> {
  _$NavigationStateCopyWithImpl(this._value, this._then);

  final NavigationState _value;
  // ignore: unused_field
  final $Res Function(NavigationState) _then;
}

abstract class $MainMenuCopyWith<$Res> {
  factory $MainMenuCopyWith(MainMenu value, $Res Function(MainMenu) then) =
      _$MainMenuCopyWithImpl<$Res>;
}

class _$MainMenuCopyWithImpl<$Res> extends _$NavigationStateCopyWithImpl<$Res>
    implements $MainMenuCopyWith<$Res> {
  _$MainMenuCopyWithImpl(MainMenu _value, $Res Function(MainMenu) _then)
      : super(_value, (v) => _then(v as MainMenu));

  @override
  MainMenu get _value => super._value as MainMenu;
}

class _$MainMenu extends MainMenu with DiagnosticableTreeMixin {
  const _$MainMenu() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.main_menu()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'NavigationState.main_menu'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is MainMenu);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result main_menu(),
    @required Result in_game(GameService gameService),
  }) {
    assert(main_menu != null);
    assert(in_game != null);
    return main_menu();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result main_menu(),
    Result in_game(GameService gameService),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (main_menu != null) {
      return main_menu();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result main_menu(MainMenu value),
    @required Result in_game(InGame value),
  }) {
    assert(main_menu != null);
    assert(in_game != null);
    return main_menu(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result main_menu(MainMenu value),
    Result in_game(InGame value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (main_menu != null) {
      return main_menu(this);
    }
    return orElse();
  }
}

abstract class MainMenu extends NavigationState {
  const MainMenu._() : super._();
  const factory MainMenu() = _$MainMenu;
}

abstract class $InGameCopyWith<$Res> {
  factory $InGameCopyWith(InGame value, $Res Function(InGame) then) =
      _$InGameCopyWithImpl<$Res>;
  $Res call({GameService gameService});
}

class _$InGameCopyWithImpl<$Res> extends _$NavigationStateCopyWithImpl<$Res>
    implements $InGameCopyWith<$Res> {
  _$InGameCopyWithImpl(InGame _value, $Res Function(InGame) _then)
      : super(_value, (v) => _then(v as InGame));

  @override
  InGame get _value => super._value as InGame;

  @override
  $Res call({
    Object gameService = freezed,
  }) {
    return _then(InGame(
      gameService: gameService == freezed
          ? _value.gameService
          : gameService as GameService,
    ));
  }
}

class _$InGame extends InGame with DiagnosticableTreeMixin {
  const _$InGame({@required this.gameService})
      : assert(gameService != null),
        super._();

  @override
  final GameService gameService;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.in_game(gameService: $gameService)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.in_game'))
      ..add(DiagnosticsProperty('gameService', gameService));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is InGame &&
            (identical(other.gameService, gameService) ||
                const DeepCollectionEquality()
                    .equals(other.gameService, gameService)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(gameService);

  @override
  $InGameCopyWith<InGame> get copyWith =>
      _$InGameCopyWithImpl<InGame>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result main_menu(),
    @required Result in_game(GameService gameService),
  }) {
    assert(main_menu != null);
    assert(in_game != null);
    return in_game(gameService);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result main_menu(),
    Result in_game(GameService gameService),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (in_game != null) {
      return in_game(gameService);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result main_menu(MainMenu value),
    @required Result in_game(InGame value),
  }) {
    assert(main_menu != null);
    assert(in_game != null);
    return in_game(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result main_menu(MainMenu value),
    Result in_game(InGame value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (in_game != null) {
      return in_game(this);
    }
    return orElse();
  }
}

abstract class InGame extends NavigationState {
  const InGame._() : super._();
  const factory InGame({@required GameService gameService}) = _$InGame;

  GameService get gameService;
  $InGameCopyWith<InGame> get copyWith;
}
