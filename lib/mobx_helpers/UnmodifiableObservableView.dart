import 'dart:collection';

import 'package:mobx/mobx.dart';

class UnmodifiableObservableListView<E> extends UnmodifiableListView<E>
    with Listenable<ListChange<E>> {
  final ObservableList<E> _source;
  UnmodifiableObservableListView(ObservableList<E> source)
      : _source = source,
        super(source);

  @override
  observe(listener, {bool fireImmediately}) {
    return _source.observe(listener, fireImmediately: fireImmediately);
  }
}

class UnmodifiableObservableSetView<E> extends UnmodifiableListView<E>
    with Listenable<SetChange<E>> {
  final ObservableSet<E> _source;
  UnmodifiableObservableSetView(ObservableSet<E> source)
      : _source = source,
        super(source);

  @override
  observe(listener, {bool fireImmediately}) {
    return _source.observe(listener, fireImmediately: fireImmediately);
  }
}

class UnmodifiableObservableMapView<K,V> extends UnmodifiableMapView<K,V>
    with Listenable<MapChange<K, V>> {
  final ObservableMap<K,V> _source;
  UnmodifiableObservableMapView(ObservableMap<K,V> source)
      : _source = source,
        super(source);

  @override
  observe(listener, {bool fireImmediately}) {
    return _source.observe(listener, fireImmediately: fireImmediately);
  }
}
