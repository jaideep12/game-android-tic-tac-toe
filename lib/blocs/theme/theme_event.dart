import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class ThemeEvent extends Equatable {
  const ThemeEvent();
}

class ThemeModeChanged extends ThemeEvent {
  final ThemeMode newThemeMode;

  ThemeModeChanged(this.newThemeMode);
  @override
  List<Object> get props => [newThemeMode];
}

class LightThemeChanged extends ThemeEvent {
  final ThemeData newThemeData;

  LightThemeChanged(this.newThemeData);
  @override
  List<Object> get props => [newThemeData];
}

class DarkThemeChanged extends LightThemeChanged{
  DarkThemeChanged(ThemeData newThemeData) : super(newThemeData);
}
