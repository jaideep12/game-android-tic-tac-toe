import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ThemeState extends Equatable {
  static ButtonThemeData getDefaultButtonTheme(ColorScheme colorScheme) =>
      ButtonThemeData(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
          side: BorderSide(color: colorScheme.onBackground),
        ),
        colorScheme: colorScheme,
        buttonColor: colorScheme.secondary,
        disabledColor: colorScheme.secondaryVariant,
        textTheme: ButtonTextTheme.primary,
        padding: EdgeInsets.all(15.0),
        alignedDropdown: true,
        splashColor: colorScheme.primaryVariant,
      );

  static ColorScheme get defaultDarkColorScheme => ColorScheme.dark().copyWith(
        brightness: Brightness.dark,
        primary: Colors.deepOrange,
        primaryVariant: Colors.red,
        onPrimary: Colors.blue,
        secondary: Colors.indigoAccent,
        secondaryVariant: Colors.indigo,
        onSecondary: Colors.blue,
        surface: Colors.black87,
        onSurface: Colors.grey,
        background: Colors.black12,
        onBackground: Colors.blueGrey,
        error: Colors.red,
        onError: Colors.pinkAccent,
      );
  static ColorScheme get defaultLightColorScheme =>
      ColorScheme.light().copyWith(
        brightness: Brightness.light,
        primary: Colors.deepOrange,
        primaryVariant: Colors.red,
        onPrimary: Colors.blue,
        secondary: Colors.indigoAccent,
        secondaryVariant: Colors.indigo,
        onSecondary: Colors.blue,
        surface: Colors.white,
        onSurface: Colors.grey,
        background: Colors.white,
        onBackground: Colors.blueGrey,
        error: Colors.red,
        onError: Colors.pinkAccent,
      );

  static ThemeData _processTheme(ThemeData themeData) => themeData.copyWith(
        buttonTheme: getDefaultButtonTheme(themeData.colorScheme),
        splashColor: themeData.colorScheme.primaryVariant,
      );

  static ThemeData get defaultLightTheme => _processTheme(
        ThemeData.from(
          colorScheme: defaultLightColorScheme,
          textTheme: ThemeData.light().textTheme,
        ),
      );
  static ThemeData get defaultDarkTheme => _processTheme(
        ThemeData.from(
          colorScheme: defaultDarkColorScheme,
          textTheme: ThemeData.dark().textTheme,
        ),
      );

  static ThemeMode get defaultThemeMode => ThemeMode.system;

  final ThemeData lightTheme, darkTheme;
  final ThemeMode themeMode;

  ThemeData currentTheme(BuildContext context) =>
      Theme.of(context).brightness == Brightness.light ? lightTheme : darkTheme;

  ThemeState.defaultTheme()
      : this(defaultLightTheme, defaultDarkTheme, defaultThemeMode);
  ThemeState(this.lightTheme, this.darkTheme, this.themeMode);

  ThemeState copyWith({
    ThemeData lightTheme,
    ThemeData darkTheme,
    ThemeMode themeMode,
  }) =>
      ThemeState(
        lightTheme ?? this.lightTheme,
        darkTheme ?? this.darkTheme,
        themeMode ?? this.themeMode,
      );

  @override
  List<Object> get props => [
        lightTheme,
        darkTheme,
        themeMode,
      ];
}
