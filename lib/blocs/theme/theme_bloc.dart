import 'dart:async';
import 'package:bloc/bloc.dart';
import './bloc.dart';

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  @override
  ThemeState get initialState => ThemeState.defaultTheme();

  @override
  Stream<ThemeState> mapEventToState(ThemeEvent event) async* {
    if (event is ThemeModeChanged) {
      yield state.copyWith(themeMode: event.newThemeMode);
    }
    else if(event is LightThemeChanged) {
      yield state.copyWith(lightTheme: event.newThemeData);
    }
    else if(event is DarkThemeChanged) {
      yield state.copyWith(darkTheme: event.newThemeData);
    }
  }
}
