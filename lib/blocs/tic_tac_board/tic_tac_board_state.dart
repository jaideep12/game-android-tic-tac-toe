part of 'tic_tac_board_bloc.dart';

@immutable
class TicTacBoardState extends Equatable {
  final BoardRepository boardRepository;
  TicTacBoardState(this.boardRepository);

  @override
  List<Object> get props => [boardRepository];
}
