part of 'tic_tac_board_bloc.dart';

abstract class TicTacBoardEvent {}

class TicTacBoardInputEvent extends TicTacBoardEvent implements EquatableMixin {
  final Point atBoardPoint;

  TicTacBoardInputEvent(this.atBoardPoint);

  @override
  List<Object> get props => [atBoardPoint];
  @override
  bool get stringify => true;
}
