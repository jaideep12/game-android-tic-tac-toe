import 'dart:async';
import 'dart:math';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:tic_tac_toe/repositories/BoardRepository.dart';
import 'package:tic_tac_toe/services/GameService.dart';

part 'tic_tac_board_event.dart';

part 'tic_tac_board_state.dart';

class TicTacBoardBloc extends Bloc<TicTacBoardEvent, TicTacBoardState> {
  final GameService gameService;

  TicTacBoardBloc(this.gameService);

  @override
  TicTacBoardState get initialState => TicTacBoardState(gameService.boardRepository);
  @override
  Stream<TicTacBoardState> mapEventToState(TicTacBoardEvent event) async* {
    if (event is TicTacBoardInputEvent) {
      //gameService.doInputAt(event.atBoardPoint);
    }
  }
}
