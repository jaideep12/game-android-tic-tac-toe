// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ScoreRepository.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ScoreRepository<EntityType extends Entity,
        ScoreType extends Comparable<dynamic>>
    on _ScoreRepository<EntityType, ScoreType>, Store {
  Computed<UnmodifiableObservableMapView<EntityType, ScoreType>>
      _$scoresComputed;

  @override
  UnmodifiableObservableMapView<EntityType, ScoreType> get scores =>
      (_$scoresComputed ??=
              Computed<UnmodifiableObservableMapView<EntityType, ScoreType>>(
                  () => super.scores,
                  name: '_ScoreRepository.scores'))
          .value;

  final _$_ScoreRepositoryActionController =
      ActionController(name: '_ScoreRepository');

  @override
  int addEntities(Iterable<EntityType> entities, {ScoreType initialScore}) {
    final _$actionInfo = _$_ScoreRepositoryActionController.startAction(
        name: '_ScoreRepository.addEntities');
    try {
      return super.addEntities(entities, initialScore: initialScore);
    } finally {
      _$_ScoreRepositoryActionController.endAction(_$actionInfo);
    }
  }

  @override
  bool removeEntity(EntityType entity) {
    final _$actionInfo = _$_ScoreRepositoryActionController.startAction(
        name: '_ScoreRepository.removeEntity');
    try {
      return super.removeEntity(entity);
    } finally {
      _$_ScoreRepositoryActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearScoreboard() {
    final _$actionInfo = _$_ScoreRepositoryActionController.startAction(
        name: '_ScoreRepository.clearScoreboard');
    try {
      return super.clearScoreboard();
    } finally {
      _$_ScoreRepositoryActionController.endAction(_$actionInfo);
    }
  }

  @override
  bool setScore(EntityType entity, ScoreType score) {
    final _$actionInfo = _$_ScoreRepositoryActionController.startAction(
        name: '_ScoreRepository.setScore');
    try {
      return super.setScore(entity, score);
    } finally {
      _$_ScoreRepositoryActionController.endAction(_$actionInfo);
    }
  }

  @override
  bool resetScore(EntityType entity) {
    final _$actionInfo = _$_ScoreRepositoryActionController.startAction(
        name: '_ScoreRepository.resetScore');
    try {
      return super.resetScore(entity);
    } finally {
      _$_ScoreRepositoryActionController.endAction(_$actionInfo);
    }
  }

  @override
  void resetAllScores() {
    final _$actionInfo = _$_ScoreRepositoryActionController.startAction(
        name: '_ScoreRepository.resetAllScores');
    try {
      return super.resetAllScores();
    } finally {
      _$_ScoreRepositoryActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
scores: ${scores}
    ''';
  }
}
