// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'BoardRepository.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BoardRepository on _BoardRepository, Store {
  Computed<UnmodifiableObservableListView<Optional<TeamModel>>> _$gridComputed;

  @override
  UnmodifiableObservableListView<Optional<TeamModel>> get grid =>
      (_$gridComputed ??=
              Computed<UnmodifiableObservableListView<Optional<TeamModel>>>(
                  () => super.grid,
                  name: '_BoardRepository.grid'))
          .value;

  final _$_BoardRepositoryActionController =
      ActionController(name: '_BoardRepository');

  @override
  bool setTeamAt(Point<num> point, TeamModel team, {bool forceUpdate = false}) {
    final _$actionInfo = _$_BoardRepositoryActionController.startAction(
        name: '_BoardRepository.setTeamAt');
    try {
      return super.setTeamAt(point, team, forceUpdate: forceUpdate);
    } finally {
      _$_BoardRepositoryActionController.endAction(_$actionInfo);
    }
  }

  @override
  bool clearBoardPoint(Point<num> point) {
    final _$actionInfo = _$_BoardRepositoryActionController.startAction(
        name: '_BoardRepository.clearBoardPoint');
    try {
      return super.clearBoardPoint(point);
    } finally {
      _$_BoardRepositoryActionController.endAction(_$actionInfo);
    }
  }

  @override
  int clearBoard() {
    final _$actionInfo = _$_BoardRepositoryActionController.startAction(
        name: '_BoardRepository.clearBoard');
    try {
      return super.clearBoard();
    } finally {
      _$_BoardRepositoryActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
grid: ${grid}
    ''';
  }
}
