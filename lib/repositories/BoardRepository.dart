import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'package:optional/optional.dart';
import 'package:tic_tac_toe/mobx_helpers/UnmodifiableObservableView.dart';
import 'package:tic_tac_toe/models/TeamModel.dart';
import 'package:tic_tac_toe/math_helper.dart';

part 'BoardRepository.g.dart';

class BoardRepository = _BoardRepository with _$BoardRepository;

abstract class _BoardRepository with Store {
  final Rectangle<int> boardRectangle;
  final ObservableList<Optional<TeamModel>> _grid;

  /// Creates a new BoardModel.
  _BoardRepository({@required Rectangle<int> boardRectangle})
      : boardRectangle = boardRectangle,
        _grid = ObservableList.of(
          List.filled(
            boardRectangle.pointCount,
            const Optional.empty(),
            growable: false,
          ),
        );

  @computed
  UnmodifiableObservableListView<Optional<TeamModel>> get grid =>
      UnmodifiableObservableListView(_grid);

  /// Returns the team at the given board point.
  /// Returns Optional.empty() if no team at given point.
  /// Throws argument error if given point not on board.
  Optional<TeamModel> getTeamAt(Point point) {
    if (!boardRectangle.containsPoint(point)) return const Optional.empty();
    return _grid[boardRectangle.getRowMajorIndexOfPoint(point)];
  }

  /// Sets the team at given board point, if the given point was empty (forceUpdate=false).
  /// If forceUpdate is true, updates even non-empty cells.
  /// Returns true if board point was changed.
  /// Throws argument error if given point not on board.
  @action
  bool setTeamAt(Point point, TeamModel team, {bool forceUpdate = false}) {
    if (!boardRectangle.containsPoint(point)) return false;
    var idx = boardRectangle.getRowMajorIndexOfPoint(point);
    if (_grid[idx].isNotEmpty && _grid[idx].value == team)
      return false;
    else if (_grid[idx].isEmpty || forceUpdate) {
      _grid[idx] = Optional.of(team);
      return true;
    } else {
      // _grid is not empty and force update is false.
      return false;
    }
  }

  /// Sets a board point to empty.
  /// Returns true if board point was changed.
  /// Throws argument error if given point not on board.
  @action
  bool clearBoardPoint(Point point) {
    if (!boardRectangle.containsPoint(point)) return false;
    var idx = boardRectangle.getRowMajorIndexOfPoint(point);
    if (_grid[idx].isEmpty)
      return false;
    else {
      _grid[idx] = const Optional.empty();
      return true;
    }
  }

  /// Clears the board and sets all _grid positions to Optional.empty().
  /// Returns the number of entries changed.
  @action
  int clearBoard() {
    int ret = 0;
    for (int i = 0; i < _grid.length; ++i) {
      if (_grid[i].isNotEmpty) {
        ++ret;
        _grid[i] = const Optional.empty();
      }
    }
    return ret;
  }
}
