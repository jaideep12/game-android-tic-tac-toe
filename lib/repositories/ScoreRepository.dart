import 'dart:collection';

import 'package:mobx/mobx.dart';
import 'package:optional/optional.dart';
import 'package:sortedmap/sortedmap.dart';
import 'package:tic_tac_toe/mobx_helpers/UnmodifiableObservableView.dart';
import 'package:tic_tac_toe/models/Entity.dart';
import 'package:flutter/foundation.dart';

part 'ScoreRepository.g.dart';

class ScoreRepository<EntityType extends Entity,
        ScoreType extends Comparable> = _ScoreRepository<EntityType, ScoreType>
    with _$ScoreRepository<EntityType, ScoreType>;

abstract class _ScoreRepository<EntityType extends Entity,
    ScoreType extends Comparable> with Store {
  final ObservableMap<EntityType, ScoreType> _scoreboard;
  final ScoreType initialScore;

  _ScoreRepository({
    @required this.initialScore,
    Iterable<EntityType> entities,
  }) : _scoreboard = ObservableMap.of(SortedMap(Ordering.byValue())) {
    if (entities != null) addEntities(entities);
  }

  @computed
  UnmodifiableObservableMapView<EntityType, ScoreType> get scores =>
      UnmodifiableObservableMapView(_scoreboard);

  /// Adds new entities to the scoreboard if they are not already in the scoreboard.
  /// For entities already in the scoreboard, does nothing.
  /// Returns the number of entities added.
  @action
  int addEntities(Iterable<EntityType> entities, {ScoreType initialScore}) {
    int i = 0;
    for (var entity in entities) {
      if (_scoreboard.containsKey(entity)) continue;
      _scoreboard[entity] = initialScore ?? this.initialScore;
      ++i;
    }
    return i;
  }

  /// Removes the entity from the scoreboard, does nothing if entity not in scoreboard.
  /// Returns true if a entity was removed.
  @action
  bool removeEntity(EntityType entity) {
    return _scoreboard.remove(entity) != null;
  }

  /// Removes all entries from the score board.
  @action
  void clearScoreboard() {
    _scoreboard.clear();
  }

  /// Returns the score of a entity, returns Optional.empty() if entity not in scoreboard.
  Optional<ScoreType> getScore(EntityType entity) {
    return Optional.of(_scoreboard[entity]);
  }

  /// Sets score of a entity to the given score.
  /// Returns true if entity's score was changed, if entity not in scoreboard returns false.
  @action
  bool setScore(EntityType entity, ScoreType score) {
    if (!_scoreboard.containsKey(entity))
      return false;
    else {
      _scoreboard[entity] = score;
      return true;
    }
  }

  /// Calls setScore for entity with initialScore.
  /// Returns true if entity's score was changed, if entity not in scoreboard returns false.
  @action
  bool resetScore(EntityType entity) {
    return setScore(entity, initialScore);
  }

  /// Resets scores of all entities to initialScore.
  @action
  void resetAllScores() {
    _scoreboard.updateAll((entity, currentScore) => initialScore);
  }
}
