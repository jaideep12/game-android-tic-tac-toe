import 'dart:collection';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'package:optional/optional.dart';
import 'package:tic_tac_toe/models/PlayerModel.dart';
import 'package:tic_tac_toe/models/TeamModel.dart';

part 'PlayersTeamsRepository.g.dart';

class PlayersTeamsRepository = _PlayersTeamsRepository
    with _$PlayersTeamsRepository;

abstract class PlayersTeamsRepositoryEvent {}

enum TeamChangeEventType {
  TEAM_ADDED,
  TEAM_REMOVED,
  TEAM_CLEARED,
}

enum PlayerChangeEventType {
  PLAYER_ADDED,
  PLAYER_REMOVED,
  PLAYER_SWITCHED_TEAM,
}

class TeamChangeEvent extends PlayersTeamsRepositoryEvent {
  final TeamModel team;
  final TeamChangeEventType eventType;
  TeamChangeEvent(this.team, this.eventType);
}

class PlayerChangeEvent extends PlayersTeamsRepositoryEvent {
  final PlayerModel player;
  final PlayerChangeEventType eventType;

  /// sourceTeam == null if PLAYER_ADDED, targetTeam == null if PLAYER_REMOVED
  final TeamModel sourceTeam, targetTeam;
  PlayerChangeEvent(
    this.player,
    this.eventType,
    this.sourceTeam,
    this.targetTeam,
  );
}

abstract class _PlayersTeamsRepository with Store {
  final ObservableSet<PlayerModel> _players;
  final ObservableMap<PlayerModel, TeamModel> _playerTeamMapping;
  final ObservableMap<TeamModel, ObservableSet<PlayerModel>>
      _teamPlayerListMapping;

  @observable
  PlayersTeamsRepositoryEvent _changeEvent;
  @computed
  PlayersTeamsRepositoryEvent get changeEvent => _changeEvent;

  _PlayersTeamsRepository({
    @required Iterable<TeamModel> teams,
    @required Iterable<PlayerModel> players,
  })  : _players = ObservableSet.of(players),
        _playerTeamMapping = ObservableMap(),
        _teamPlayerListMapping = ObservableMap.of(
          Map.fromIterable(
            teams,
            key: (team) => team,
            value: (team) => ObservableSet(),
          ),
        );

  @computed
  Iterable<TeamModel> get teams => _teamPlayerListMapping.keys;
  @computed
  Iterable<PlayerModel> get players => UnmodifiableListView(_players);

  /// Returns empty optional if player not in list of players or not in any team.
  Optional<TeamModel> getPlayerTeam(PlayerModel player) {
    return _playerTeamMapping[player].toOptional;
  }

  /// Returns empty optional if team not in list of teams.
  Optional<ObservableSet<PlayerModel>> getTeamPlayerList(TeamModel team) {
    return _teamPlayerListMapping[team].toOptional;
  }

  /// Adds a new team to the team player list mapping, does nothing if team already present.
  /// Returns true if a new team was added, false otherwise.
  @action
  bool addNewTeam(TeamModel team) {
    if (_teamPlayerListMapping.containsKey(team)) return false;
    _teamPlayerListMapping[team] = ObservableSet();
    log("New team registered: $team.");
    _changeEvent = TeamChangeEvent(team, TeamChangeEventType.TEAM_ADDED);
    return true;
  }

  /// Adds new player to list of players if not already in the list.
  /// Can also optionally register player to team.
  /// If team is not in list of teams, it is added to the list of teams.
  /// Returns true if a new player was added, false otherwise.
  @action
  bool addNewPlayer(PlayerModel player, {TeamModel team}) {
    if (player == null) return false;

    /// return if player already in list of players
    if (!_players.add(player)) return false;
    if (team != null) {
      addNewTeam(team);
      movePlayerToTeam(player, team);
    }
    log("New player registered: $player ${team != null ? ', team: $team' : ''}");
    _changeEvent = PlayerChangeEvent(
        player, PlayerChangeEventType.PLAYER_ADDED, null, team);
    return true;
  }

  /// Removes player from the game state.
  /// Returns true if a player was removed, false otherwise.
  @action
  bool removePlayer(PlayerModel player) {
    TeamModel currentTeam = _playerTeamMapping[player];
    if (!_players.remove(player)) return false;
    removePlayerFromTeam(player);
    _changeEvent = PlayerChangeEvent(
        player, PlayerChangeEventType.PLAYER_REMOVED, currentTeam, null);
    return true;
  }

  /// Removes a player from its team, does nothing if player not in any team.
  /// Returns true if a player was removed from the team, false otherwise.
  @action
  bool removePlayerFromTeam(PlayerModel player) {
    Optional<TeamModel> currentTeam = _playerTeamMapping[player].toOptional;
    if (currentTeam.isEmpty) return false;
    _teamPlayerListMapping[currentTeam.value].remove(player);
    _playerTeamMapping.remove(player);
    _changeEvent = PlayerChangeEvent(
      player,
      PlayerChangeEventType.PLAYER_SWITCHED_TEAM,
      currentTeam.value,
      null,
    );
    return true;
  }

  /// Removes all players from a team, does nothing if team is empty or not in registered teams.
  /// Returns true if a player was removed from the team, false otherwise.
  @action
  bool clearTeamPlayers(TeamModel team) {
    if ((_teamPlayerListMapping[team]?.length ?? 0) == 0) return false;
    _teamPlayerListMapping[team].forEach((player) {
      _playerTeamMapping.remove(player);
    });
    _teamPlayerListMapping[team].clear();
    _changeEvent = TeamChangeEvent(team, TeamChangeEventType.TEAM_CLEARED);
    return true;
  }

  /// Moves a player to a team.
  /// If player is not in the list of players or team not in team player list map, does nothing.
  /// Returns true if a player was moved to a different team, false otherwise.
  @action
  bool movePlayerToTeam(PlayerModel player, TeamModel team) {
    if (!_players.contains(player) || !_teamPlayerListMapping.containsKey(team))
      return false;
    TeamModel currentTeam = _playerTeamMapping[player];
    // return if already in correct team
    if (currentTeam == team) return false;
    // remove player from team
    removePlayerFromTeam(player);
    // add player to new team
    _playerTeamMapping[player] = team;
    _teamPlayerListMapping[team].add(player);
    _changeEvent = PlayerChangeEvent(
      player,
      PlayerChangeEventType.PLAYER_SWITCHED_TEAM,
      currentTeam,
      team,
    );
    return true;
  }

  /// Removes a team and all players in the team become team-less.
  /// Returns true if a team was removed, false otherwise.
  @action
  bool removeTeam(TeamModel team) {
    if (!_teamPlayerListMapping.containsKey(team)) return false;
    clearTeamPlayers(team);
    _teamPlayerListMapping.remove(team);
    _changeEvent = TeamChangeEvent(team, TeamChangeEventType.TEAM_REMOVED);
    return true;
  }
}
