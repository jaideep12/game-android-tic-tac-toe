// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'PlayersTeamsRepository.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PlayersTeamsRepository on _PlayersTeamsRepository, Store {
  Computed<PlayersTeamsRepositoryEvent> _$changeEventComputed;

  @override
  PlayersTeamsRepositoryEvent get changeEvent => (_$changeEventComputed ??=
          Computed<PlayersTeamsRepositoryEvent>(() => super.changeEvent,
              name: '_PlayersTeamsRepository.changeEvent'))
      .value;
  Computed<Iterable<TeamModel>> _$teamsComputed;

  @override
  Iterable<TeamModel> get teams =>
      (_$teamsComputed ??= Computed<Iterable<TeamModel>>(() => super.teams,
              name: '_PlayersTeamsRepository.teams'))
          .value;
  Computed<Iterable<PlayerModel>> _$playersComputed;

  @override
  Iterable<PlayerModel> get players => (_$playersComputed ??=
          Computed<Iterable<PlayerModel>>(() => super.players,
              name: '_PlayersTeamsRepository.players'))
      .value;

  final _$_changeEventAtom = Atom(name: '_PlayersTeamsRepository._changeEvent');

  @override
  PlayersTeamsRepositoryEvent get _changeEvent {
    _$_changeEventAtom.reportRead();
    return super._changeEvent;
  }

  @override
  set _changeEvent(PlayersTeamsRepositoryEvent value) {
    _$_changeEventAtom.reportWrite(value, super._changeEvent, () {
      super._changeEvent = value;
    });
  }

  final _$_PlayersTeamsRepositoryActionController =
      ActionController(name: '_PlayersTeamsRepository');

  @override
  bool addNewTeam(TeamModel team) {
    final _$actionInfo = _$_PlayersTeamsRepositoryActionController.startAction(
        name: '_PlayersTeamsRepository.addNewTeam');
    try {
      return super.addNewTeam(team);
    } finally {
      _$_PlayersTeamsRepositoryActionController.endAction(_$actionInfo);
    }
  }

  @override
  bool addNewPlayer(PlayerModel player, {TeamModel team}) {
    final _$actionInfo = _$_PlayersTeamsRepositoryActionController.startAction(
        name: '_PlayersTeamsRepository.addNewPlayer');
    try {
      return super.addNewPlayer(player, team: team);
    } finally {
      _$_PlayersTeamsRepositoryActionController.endAction(_$actionInfo);
    }
  }

  @override
  bool removePlayer(PlayerModel player) {
    final _$actionInfo = _$_PlayersTeamsRepositoryActionController.startAction(
        name: '_PlayersTeamsRepository.removePlayer');
    try {
      return super.removePlayer(player);
    } finally {
      _$_PlayersTeamsRepositoryActionController.endAction(_$actionInfo);
    }
  }

  @override
  bool removePlayerFromTeam(PlayerModel player) {
    final _$actionInfo = _$_PlayersTeamsRepositoryActionController.startAction(
        name: '_PlayersTeamsRepository.removePlayerFromTeam');
    try {
      return super.removePlayerFromTeam(player);
    } finally {
      _$_PlayersTeamsRepositoryActionController.endAction(_$actionInfo);
    }
  }

  @override
  bool clearTeamPlayers(TeamModel team) {
    final _$actionInfo = _$_PlayersTeamsRepositoryActionController.startAction(
        name: '_PlayersTeamsRepository.clearTeamPlayers');
    try {
      return super.clearTeamPlayers(team);
    } finally {
      _$_PlayersTeamsRepositoryActionController.endAction(_$actionInfo);
    }
  }

  @override
  bool movePlayerToTeam(PlayerModel player, TeamModel team) {
    final _$actionInfo = _$_PlayersTeamsRepositoryActionController.startAction(
        name: '_PlayersTeamsRepository.movePlayerToTeam');
    try {
      return super.movePlayerToTeam(player, team);
    } finally {
      _$_PlayersTeamsRepositoryActionController.endAction(_$actionInfo);
    }
  }

  @override
  bool removeTeam(TeamModel team) {
    final _$actionInfo = _$_PlayersTeamsRepositoryActionController.startAction(
        name: '_PlayersTeamsRepository.removeTeam');
    try {
      return super.removeTeam(team);
    } finally {
      _$_PlayersTeamsRepositoryActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
changeEvent: ${changeEvent},
teams: ${teams},
players: ${players}
    ''';
  }
}
