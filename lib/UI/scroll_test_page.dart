import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tic_tac_toe/services/AdMobService.dart';

class ScrollTestPage extends StatelessWidget {
  Widget get _filler => const Text(
        "Filler...",
        textScaleFactor: 5,
      );

  List<Widget> _fillers({int count = 5}) => List.filled(count, _filler);

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (ctx, orientation) => ListView(
        children: <Widget>[
          ..._fillers(count: 10),
          Provider.of<AdMobService>(context).adWidgetMainMenu(context, 0),
          ..._fillers(count: 10),
          Provider.of<AdMobService>(context).adWidgetInGame(context, 0),
          ..._fillers(count: 10),
        ],
      ),
    );
  }
}
