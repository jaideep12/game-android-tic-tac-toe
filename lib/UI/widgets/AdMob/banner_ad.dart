import 'dart:developer';
import 'dart:math';

import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';

class BannerAdWidget extends StatefulWidget {
  final String adUnitId;
  final AdSize size;
  final MobileAdTargetingInfo targetingInfo;
  final MobileAdListener listener;

  final Widget error, loading;

  BannerAdWidget({
    @required this.adUnitId,
    @required this.size,
    this.targetingInfo,
    this.listener,
    @required this.error,
    @required this.loading,
  }) : super(key: GlobalKey());

  @override
  _BannerAdWidgetState createState() => _BannerAdWidgetState(
        adUnitId: adUnitId,
        size: size,
        listener: listener,
        targetingInfo: targetingInfo,
      );
}

class _BannerAdWidgetState extends State<BannerAdWidget>
    with WidgetsBindingObserver {
  BannerAd ad;
  MobileAdEvent event;
  bool isLoaded = false;
  Rect _prevMyPos;

  _BannerAdWidgetState({
    @required String adUnitId,
    @required AdSize size,
    MobileAdTargetingInfo targetingInfo,
    MobileAdListener listener,
  }) {
    ad = BannerAd(
        adUnitId: adUnitId,
        size: size,
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          if (widget.listener != null) widget.listener(event);
          setState(() {
            this.isLoaded = this.isLoaded || event == MobileAdEvent.loaded;
            this.event = event;
          });
        })
      ..load();
  }

  Point<int> getBannerDim(BuildContext context, AdSize size) {
    if (size.adSizeType != AdSizeType.SmartBanner)
      return Point(
        size.width,
        size.height,
      );
    double screenH = MediaQuery.of(context).size.height;
    double screenW = MediaQuery.of(context).size.width;
    int adH;
    if (screenH <= 400)
      adH = 32;
    else if (screenH <= 720)
      adH = 50;
    else
      adH = 90;
    return Point(screenW.toInt(), adH);
  }

  Rect get myPos {
    var key = widget.key as GlobalKey;
    final renderObject = key?.currentContext?.findRenderObject();
    var translation = renderObject?.getTransformTo(null)?.getTranslation();
    if (translation != null && renderObject.paintBounds != null) {
      return renderObject.paintBounds
          .shift(Offset(translation.x, translation.y));
    } else
      return null;
  }

  void scheduleAdRender(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Rect paintBounds = myPos;
      if (paintBounds != null && paintBounds != _prevMyPos) {
        ad.show(
          anchorType: AnchorType.top,
          anchorOffset: paintBounds.top,
          horizontalCenterOffset: paintBounds.left +
              paintBounds.width / 2 -
              MediaQuery.of(context).size.width / 2,
        );
        _prevMyPos = paintBounds;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Point<int> dim = getBannerDim(context, ad.size);
    if (isLoaded) scheduleAdRender(context);
    return SizedBox(
      width: dim.x.toDouble(),
      height: dim.y.toDouble(),
      child: isLoaded
          ? const Center(child: Text("Ad displayed here."))
          : event == MobileAdEvent.failedToLoad ? widget.error : widget.loading,
    );
  }

  @override
  void dispose() {
    ad?.dispose();
    super.dispose();
  }
}
