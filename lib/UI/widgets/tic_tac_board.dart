import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:tic_tac_toe/services/GameService.dart';

class TicTacBoard extends StatelessWidget {
  final GameService gameService;

  const TicTacBoard({Key key, @required this.gameService}) : super(key: key);

  Widget _emptyCell() {
    return const Text("");
    return const FittedBox(fit: BoxFit.fill, child: AutoSizeText("X"));
  }

  Widget _gridImageBuilder({
    @required BuildContext context,
    @required Point boardPoint,
  }) {
    return GestureDetector(
        onTap: () => gameService.doTurn(boardPoint),
        child: Observer(
          name: "BoardPoint $boardPoint",
          builder: (context) {
            var team = gameService.boardRepository.getTeamAt(boardPoint);
            return team.isEmpty ? _emptyCell() : team.value.teamImage;
          },
        ));
  }

  Widget _getGridBox({
    @required BuildContext context,
    @required Point boardPoint,
  }) {
    ThemeData theme = Theme.of(context);
    return AspectRatio(
      aspectRatio: 1,
      child: Container(
        child: _gridImageBuilder(
          context: context,
          boardPoint: boardPoint,
        ),
      ),
    );
  }

  Widget _getGrid(BuildContext context, Rectangle boardRect) {
    return Table(
      border: TableBorder.symmetric(
        inside: BorderSide(
          width: 10.0,
          color: Theme.of(context).accentColor,
        ),
      ),
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: List.generate(
        boardRect.height + 1,
        (y) => TableRow(
          children: List.generate(
            boardRect.width + 1,
            (x) => Padding(
              padding: EdgeInsets.all(7.0),
              child: _getGridBox(
                context: context,
                boardPoint: Point(x + boardRect.left, y + boardRect.top),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    print("New board");
    return AspectRatio(
      aspectRatio: 1.0,
      child: Card(
        borderOnForeground: true,
        elevation: 10,
        child: Padding(
          padding: EdgeInsets.all(7),
          child: _getGrid(
            context,
            gameService.boardRepository.boardRectangle,
          ),
        ),
      ),
    );
  }
}
