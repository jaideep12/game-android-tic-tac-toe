import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';
import 'package:tic_tac_toe/UI/widgets/tic_tac_board.dart';
import 'package:tic_tac_toe/models/GameState.dart';
import 'package:tic_tac_toe/services/AdMobService.dart';
import 'package:tic_tac_toe/services/AppService.dart';
import 'package:tic_tac_toe/services/GameService.dart';

class InGamePage extends StatelessWidget {
  Widget _header(BuildContext context) {
    var service = Provider.of<GameService>(context);
    return Observer(
      name: "In game header",
      builder: (context) => AnimatedSwitcher(
        duration: Duration(milliseconds: 400),
        child: service.gameState is GameEnded
            ? (service.gameState as GameEnded).winners.length <= 1
                ? _winnerDisplay(context, service.gameState)
                : _drawDisplay(context, service.gameState)
            : _currentTurnDisplay(),
      ),
    );
  }

  Widget _drawDisplay(BuildContext context, GameEnded gameState) {
    return FittedBox(
      fit: BoxFit.fitWidth,
      child: AutoSizeText(
        "Draw",
        textScaleFactor: 6,
      ),
    );
  }

  Widget _winnerDisplay(BuildContext context, GameEnded gameState) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Flexible(
          flex: 20,
          fit: FlexFit.tight,
          child: FittedBox(
            fit: BoxFit.fill,
            child: AutoSizeText(
              "Winner",
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 5),
        ),
        Flexible(
          flex: 7,
          child: gameState.winners.first.teamImage,
        ),
      ],
    );
  }

  Widget _currentTurnDisplay() {
    return Observer(
      name: "Current turn team",
      builder: (context) {
        var gameService = Provider.of<GameService>(context);
        var service = gameService.playerTurnService;
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Flexible(
              flex: 7,
              child: service.currentTurnTeam.teamImage,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
            ),
            Flexible(
              flex: 5,
              fit: FlexFit.tight,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Flexible(
                    flex: 1,
                    child: FittedBox(
                      fit: BoxFit.fill,
                      child: AutoSizeText(
                        "Team: ${service.currentTurnTeam.name}",
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: FittedBox(
                      fit: BoxFit.fill,
                      child: AutoSizeText(
                        "${service.currentTurnPlayer.name}",
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  Widget _endgameMenu(BuildContext context) {
    return Container(
      color: Colors.black.withOpacity(0.7),
      child: ButtonBar(
        alignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          MaterialButton(
            color: Theme.of(context).toggleableActiveColor,
            elevation: 10.0,
            onPressed: () =>
                Provider.of<AppService>(context, listen: false).goBack(),
            child: Icon(
              Icons.arrow_left,
              size: 100,
            ),
          ),
          MaterialButton(
            elevation: 10.0,
            color: Theme.of(context).toggleableActiveColor,
            onPressed: () =>
                Provider.of<GameService>(context, listen: false).resetGame(),
            child: Icon(
              Icons.replay,
              size: 100,
            ),
          ),
        ],
      ),
    );
  }

  Widget _boardWidget() {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 5,
      ),
      child: AspectRatio(
        aspectRatio: 1,
        child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          children: <Widget>[
            Observer(
              name: "Tic Tac Board Ignore Pointer observer",
              builder: (context) => IgnorePointer(
                ignoring:
                    Provider.of<GameService>(context).gameState is GameEnded,
                child: Provider.of<TicTacBoard>(context),
              ),
            ),
            Observer(
              name: "End Game popup observer",
              builder: (context) {
                var gameState = Provider.of<GameService>(context).gameState;
                return IgnorePointer(
                  ignoring: gameState is! GameEnded,
                  child: AnimatedOpacity(
                    opacity: gameState is GameEnded ? 1 : 0,
                    duration: Duration(milliseconds: 500),
                    child: _endgameMenu(context),
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }

  Widget _scoreWidget() {
    return Card(
      elevation: 10,
      margin: EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 10,
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: 20,
          horizontal: 10,
        ),
        child: Observer(
          name: "Score widget",
          builder: (context) {
            print("Score widget build.");
            var gameService = Provider.of<GameService>(context);
            var scores = gameService.scoreRepository;
            var teams = gameService.playersTeamsRepository.teams;
            List<DataRow> dataRows = teams
                .map(
                  (team) => DataRow(
                    selected: false,
                    cells: <DataCell>[
                      DataCell(team.teamImage),
                      DataCell(
                        Observer(
                          name: "${team.name}'s score",
                          builder: (context) {
                            return Text(
                              scores.getScore(team).value.toString(),
                              textScaleFactor: 3.45,
                              textAlign: TextAlign.center,
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                )
                .toList(growable: false);
            return DataTable(
              columns: <DataColumn>[
                DataColumn(
                  label: Center(child: Text("Team", textScaleFactor: 3)),
                  numeric: false,
                ),
                DataColumn(
                  label: Center(child: Text("Wins", textScaleFactor: 3)),
                  numeric: true,
                ),
              ],
              rows: dataRows,
            );
          },
        ),
      ),
    );
  }

  Widget _adCard(BuildContext context) {
    return Card(
      elevation: 20,
      margin: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 5,
      ),
      child: Padding(
          padding: EdgeInsets.all(20),
          child: LimitedBox(
            maxHeight: 90,
            child:
                Provider.of<AdMobService>(context).adWidgetInGame(context, 0),
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ProxyProvider<GameService, TicTacBoard>(
      update: (context, gameService, previous) => TicTacBoard(
        gameService: gameService,
      ),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Flexible(
              flex: 2,
              fit: FlexFit.loose,
              child: Padding(
                padding: EdgeInsets.only(
                  top: 10,
                ),
                child: _header(context),
              ),
            ),
            Flexible(
              flex: 14,
              fit: FlexFit.loose,
              child: _boardWidget(),
            ),
            Flexible(
              flex: 2,
              fit: FlexFit.loose,
              child: _adCard(context),
            ),
            Flexible(
              flex: 2,
              fit: FlexFit.loose,
              child: _scoreWidget(),
            ),
          ],
        ),
      ),
    );
  }
}
