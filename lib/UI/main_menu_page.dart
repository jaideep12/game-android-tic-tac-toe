import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:tic_tac_toe/blocs/theme/bloc.dart';
import 'package:tic_tac_toe/blocs/theme/theme_bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:tic_tac_toe/services/AdMobService.dart';
import 'package:tic_tac_toe/services/AppService.dart';

class MainMenuPage extends StatelessWidget {
  Widget getBanner(BuildContext context) => FittedBox(
        fit: BoxFit.fitWidth,
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: 5,
            horizontal: 5,
          ),
          child: AutoSizeText(
            "Tic Tac Toe",
          ),
        ),
      );

  Widget getMenuButton(BuildContext context, String key,
          {@required String text, @required VoidCallback onPressed}) =>
      Container(
        margin: EdgeInsets.symmetric(vertical: 5.0),
        child: FlatButton(
          key: Key(key),
          child: FittedBox(
              fit: BoxFit.fitHeight,
              child: AutoSizeText(
                text,
                textScaleFactor: 1.5,
              )),
          onPressed: onPressed,
        ),
      );

  Widget getMenuContainer(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 5,
        horizontal: 20,
      ),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
//          getMenuButton(
//            context,
//            "button_vs_cpu",
//            text: "vs CPU",
//            onPressed: () {
//              Provider.of<AppService>(
//                context,
//                listen: false,
//              ).gotoSinglePlayerGame();
//            },
//          ),
          getMenuButton(
            context,
            "button_vs_local",
            text: "vs Local Offline",
            onPressed: () {
              Provider.of<AppService>(
                context,
                listen: false,
              ).gotoSinglePlayerGame();
            },
          ),
        ],
      ),
    );
  }

  Widget _themeSlider(BuildContext context) {
    return BlocBuilder<ThemeBloc, ThemeState>(
      builder: (context, themeState) => Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 3,
          vertical: 5,
        ),
        child: Column(
          children: [
            CupertinoSwitch(
              value: Theme.of(context).brightness == Brightness.light,
              onChanged: (newValue) {
                BlocProvider.of<ThemeBloc>(context).add(
                  ThemeModeChanged(newValue ? ThemeMode.light : ThemeMode.dark),
                );
              },
            ),
            Text(Theme.of(context).brightness == Brightness.light
                ? "Light Theme"
                : "Dark Theme")
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) => Center(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              getBanner(context),
              Card(
                elevation: 10,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25),
                ),
                margin: EdgeInsets.symmetric(
                  horizontal: 70,
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: 20,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      getMenuContainer(context),
                      _themeSlider(context),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 50.0),
              Provider.of<AdMobService>(context).adWidgetMainMenu(context, 0),
              SizedBox(height: 10.0),
              Provider.of<AdMobService>(context).adWidgetMainMenu(context, 1),
            ],
          ),
        ),
      );
}
